<?php
require 'DB/DBAPI.php';
require 'DB/ReportsAPI.php';
if (!Is_Logged_In()) {
redirect("login.php");
} 
else 
 {
$UserData = UserDetails($_SESSION["acc"]);
$Username = $_SESSION["Username"];
$FirstName = $UserData[0]["UserFirstName"];
$LastName = $UserData[0]["UserSurname"];
$UserType = $UserData[0]["UserType"];


$state = base64_decode($_GET["ColID"]);
if($state=="all"){
$CollecVals = getCollectionCoords();
}
else{
	$CollecVals = getCollCoord($state);
}
$i = 0;
foreach($CollecVals as $row){
$lat = $row['Latitude'];
    $lon = $row['longitude'];
    $i++;
    if($lon!=0 & $lat!=0){
        $start_lon = $lon;
        $start_lat = $lat;
    }
}
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        
        <!-- Title -->
        <title>Axis Park | <?php echo basename($_SERVER['PHP_SELF']); ?></title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />
        
        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="assets/plugins/materialize/css/materialize.min.css"/>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">        

        	
        <!-- Theme Styles -->
        <link href="assets/css/alpha.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>
        
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body>
    <?php require 'config.php'; ?>
            <main class="mn-inner" id="map-canvas">
            </main>
        </div>
        <div class="left-sidebar-hover"></div>
        
        <!-- Javascripts -->
        <script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
        <script src="assets/plugins/materialize/js/materialize.min.js"></script>
        <script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
        <script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
        <!DOCTYPE html>
        <html>
        <head>
          <meta charset="utf-8" />
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
         
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
          <script src="main.js"></script>
        </head>
        <body>
          
        </body>
        </html>
        <script
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBXYnIsUWziNd4F48t63Nh9irV2sskLPhY&callback=initMap">
        </script>
        <script src="assets/js/alpha.min.js"></script>
        <script>
       
    $(document).ready(function(){
        
  
    var customIcons = {
      C: {
        icon: 'C'
      },
      I: {
        icon: 'I'
      }
    };
        var start_lan = '<?php echo $start_lat; ?>';
         var start_lon = '<?php echo $start_lon; ?>';
		 console.log(start_lan);
   var map = new google.maps.Map(document.getElementById("map-canvas"), {
        center: new google.maps.LatLng(start_lan,start_lon),
        zoom: 16,
        mapTypeId: 'roadmap'
      });
      var infoWindow = new google.maps.InfoWindow;
		var state = '<?php echo $state; ?>';
		console.log(state);
      // Change this depending on the name of your PHP file
      downloadUrl("engines/CollecGeoLoc.php?state="+state, function(data) {
       
        var xml = data.responseXML;
        var markers = xml.documentElement.getElementsByTagName("marker");
        for (var i = 0; i < markers.length; i++) {
          var name = markers[i].getAttribute("CollectionNum");
          var type = markers[i].getAttribute("Type");
          var Total = markers[i].getAttribute("Total");
          var CustomerName = markers[i].getAttribute("CustomerName");
          var point = new google.maps.LatLng(
              parseFloat(markers[i].getAttribute("lat")),
              parseFloat(markers[i].getAttribute("lng")));
              var icon = customIcons[type] || {};
          var html = "<b>Customer Name: "+CustomerName+ "</b> <br/>"+"<b>Ref#: " + name + "</b> <br/>"+"<b>Collection Total: $"+Total+ "</b> <br/>";
          
          var marker = new google.maps.Marker({
            map: map,
            position: point,
            label: icon.icon
          });
          bindInfoWindow(marker, map, infoWindow, html);
        }
      });
      
  function bindInfoWindow(marker, map, infoWindow, html) {
      google.maps.event.addListener(marker, 'click', function() {
        infoWindow.setContent(html);
        infoWindow.open(map, marker);
      });
    }

    function downloadUrl(url, callback) {
      var request = window.ActiveXObject ?
          new ActiveXObject('Microsoft.XMLHTTP') :
          new XMLHttpRequest;

      request.onreadystatechange = function() {
        if (request.readyState === 4) {
          request.onreadystatechange = doNothing;
          callback(request, request.status);
        }
      };

      request.open('GET', url, true);
      request.send(null);
    }
    
      function doNothing() {}

});

</script>
        
    </body>
</html>