<?php
require 'DB/DBAPI.php';
if (!Is_Logged_In()) {
    redirect("login.php");
} else {
    $UserData = UserDetails($_SESSION["acc"]);
    $Username = $_SESSION["Username"];
    $FirstName = $UserData[0]["UserFirstName"];
    $LastName = $UserData[0]["UserSurname"];
    $UserType = $UserData[0]["UserType"];
    $ShiftID = $_GET["ShftNum"];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- Title -->
        <title>Axis Park | <?php echo basename($_SERVER['PHP_SELF']); ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />

        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="assets/plugins/materialize/css/materialize.min.css"/>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">
        <link href="assets/plugins/datatables/css/jquery.dataTables.min.css" rel="stylesheet">


        <!-- Theme Styles -->
        <link href="assets/css/alpha.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <?php require 'config.php'; ?>

        <main class="mn-inner">
            <div class="row">
                <form class="CancelShift col s12" method="post" >
                    <div class="row">
                        <div class="input-field col s8">
                            <input placeholder="Specify cancellation reason" id="reason" name="reason" type="text" class="validate">
                            <label for="phone">Cancel Shift</label>
                        </div>
                    </div>
                </form>
                <div class ="row">

                    <div class="col s4 l4 m4">
                        <!-- <a  class="waves-effect waves-grey btn-flat"></a>-->
                        <a type="submit"  class= "BtnCancelShift waves-effect waves-light btn blue m-b-xs">Proceed ..</a>
                    </div>
                    

                </div>
            </div>
        </main>

    </div>
    <div class="left-sidebar-hover"></div>


    <!-- Javascripts -->
    <script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
    <script src="assets/plugins/materialize/js/materialize.min.js"></script>
    <script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
    <script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
    <script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/alpha.min.js"></script>
    <script src="assets/js/pages/table-data.js"></script>
    <script>
        $(document).ready(function () {
            var name = '<?php echo $Username; ?>';
            setTimeout(function () {
                Materialize.toast('Welcome to ' + name + '!', 4000)
            }, 4000);

            $(".BtnCancelShift").click(function (ev) {
                var shftNum = '<?php echo $ShiftID; ?>';
                var cancRsn = $("#reason").val();
                ev.preventDefault();
                $.get("engines/CancShft.php?ShftNum="+shftNum+"&Rzn="+cancRsn,
                        function (response) {
                            var fdbk = $.parseJSON(response);
                            if (fdbk.status === "ok") {
                                alert(fdbk.msg);
                                var delay = 1000;
                                setTimeout(function () {
                                    window.location = "ManageShifts.php";
                                }, delay);
                            } else {
                                alert(fdbk.msg);
                            }
                        });
            });

        });
    </script>
</body>
</html>
