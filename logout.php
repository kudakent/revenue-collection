<?php
require_once 'DB/DBAPI.php';

if($_SESSION['acc']!="")
{
	redirect("index.php?acc=".$_SESSION['acc']);
}
if(isset($_GET['logout']) && $_GET['logout']=="true")
{
	logout();
	redirect("login.php");
}
if(!isset($_SESSION['acc']))
{
    redirect("login.php");
}
