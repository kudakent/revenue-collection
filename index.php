<?php 
require 'DB/DBAPI.php';
require 'DB/ReportsAPI.php';
if(!Is_Logged_In()){
    redirect("login.php");
}
else{
$UserData = UserDetails($_SESSION["acc"]);
$Username = $_SESSION["Username"];
$FirstName = $UserData[0]["UserFirstName"];
$LastName  = $UserData[0]["UserSurname"];
$UserType = $UserData[0]["UserType"];

$sale =get_today_sales(); 

 if(empty($sale)){
      $today_sales=0.00;
 }else{
   $today_sales = round($sale[0]['todaySales'],2);  
 }



//sales by marshal graph
$value = GetYearSalesByMarshal();
$i=0;
foreach($value as $val){
    $vals[$i] = "[".$i.",".round($val["InvoiceTotal"],0)."]";
   $valsNames[$i] = '['.$i.','.'"'.$val["SalesManName"].'"'.']'; //implode( ", ", $list );
    $i++;
}

@$Sales = implode(",",$vals);
@$Names = implode(",",$valsNames);


//sales by service 
$value2 = GetYearSalesByService();
$i=0;
foreach($value2 as $val){
    $vals2[$i] = "[".$i.",".round($val["InvoiceTotal"],0)."]";
   $valsProds[$i] = '['.$i.','.'"'.$val["ProductName"].'"'.']'; //implode( ", ", $list );
    $i++;
}

@$Sales2 = implode(",",$vals2);
@$Prods = implode(",",$valsProds);

}




?>
<!DOCTYPE html>
<html lang="en">
    <head>
        
        <!-- Title -->
        <title>Revenue Collection | Home</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />
        
        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="assets/plugins/materialize/css/materialize.min.css"/>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">    
        <link href="assets/plugins/metrojs/MetroJs.min.css" rel="stylesheet">
        <link href="assets/plugins/weather-icons-master/css/weather-icons.min.css" rel="stylesheet">

        	
        <!-- Theme Styles -->
        <link href="assets/css/alpha.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>
        
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body>
     
          <?php require 'config.php'; ?>
        
            <main class="mn-inner inner-active-sidebar">
                <div class="middle-content">
                    <div class="row no-m-t no-m-b">
                    <div class="col s140 m140 140">
                        <div class="card stats-card">
                            <div class="card-content">
                                <div class="card-options">
                                    <ul>
                                        <li class="red-text"><span class="badge cyan lighten-1">gross</span></li>
                                    </ul>
                                </div>
                                <span class="card-title">Sales</span>
                                <span class="stats-counter">$<span class="counter"><?php echo $today_sales; ?></span><small>Today</small></span>
                            </div>
                           
                        </div>
                    </div>
                         <div class="col s140 m140 1140">
                        <div class="card stats-card">
                            <div class="card-content">
                                <div class="card-options">
                                    <ul>
                                        <li><a href="javascript:void(0)"><i class="material-icons">more_vert</i></a></li>
                                    </ul>
                                </div>
                                <span class="card-title">MONTH TO DATE</span>
                                <span class="stats-counter">$<span class="counter"><?php echo round(get_month_sales(),2); ?></span><small>This month</small></span>
                            </div>
                            
                        </div>
                    </div>
                     <div class="col s140 m140 1140">
                        <div class="card stats-card">
                            <div class="card-content">
                                <span class="card-title">Reports</span>
                                <span class="stats-counter">$<span class="counter"><?php echo round(get_lasmon_sales(),2); ?></span><small>Last Month</small></span>
                                <div class="percent-info green-text"><?php if(get_lasmon_sales()<=0){$lasMnth = 1;} else {$lasMnth = get_lasmon_sales();} echo @round(((get_month_sales()-get_lasmon_sales())/$lasMnth)*100,2); ?>% <i class="material-icons">trending_up</i></div>
                            </div>
                           
                        </div>
                    </div>
                </div>
                    <div class="row no-m-t no-m-b">
                        
                        
                          <div class="col s12 m12 l12">
                         <div class="card">
                            <div class="card-content">
                                <span class="card-title">Sales By Service</span>
                                <div id="FlotTk2" style="height:300px;"></div>
                            </div>
                        </div>
                        </div>
                    </div>
                        
                          <div class="row no-m-t no-m-b">
                            <div class="col s6 m6 l6">
                         <div class="card">
                            <div class="card-content">
                                <span class="card-title">Sales By Marshal</span>
                                <div id="FlotTk" style="height:300px;"></div>
                            </div>
                        </div>
                        </div>  
                              
                        <div class="col s6  m6 16">
                            <div class="card server-card">
                                <div class="card-content">
                                <div class="card-options">
                                    <ul>
                                        <li class="red-text"><span class="badge blue-grey lighten-3">optimal</span></li>
                                    </ul>
                                </div>
                                    <span class="card-title">RECEIPT AREAS</span>
                                               
                                    <div class="stats-info">
                                        <ul>
                                            <?php 
                                            $Slots = GetSlots(); 
                                            foreach($Slots as $Slt){
                                              $AreaLabel = $Slt["AreaLabel"]; 
                                             $SlotSales = GetSlotsSales($AreaLabel);
                                             $MySale = $SlotSales[0]["Sls"];
                                             if($MySale==0){
                                                 $MySale = "0.00";
                                             }
                                            ?>
                                            <li><?php echo $AreaLabel; ?> <div class="percent-info green-text right">$<?php echo round($MySale,2); ?> </div></li>
                                            <?php } ?>   
                                        </ul>
                                    </div>
                                    <div id="flotchart2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                   
                </div>
                
            </main>
            
        </div>
        <div class="left-sidebar-hover"></div>
        
        
        <!-- Javascripts -->
        <script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
        <script src="assets/plugins/materialize/js/materialize.min.js"></script>
        <script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
        <script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
        <script src="assets/plugins/waypoints/jquery.waypoints.min.js"></script>
        <script src="assets/plugins/counter-up-master/jquery.counterup.min.js"></script>
        <script src="assets/plugins/jquery-sparkline/jquery.sparkline.min.js"></script>
        <script src="assets/plugins/chart.js/chart.min.js"></script>
        <script src="assets/plugins/flot/jquery.flot.min.js"></script>
        <script src="assets/plugins/flot/jquery.flot.time.min.js"></script>
        <script src="assets/plugins/flot/jquery.flot.symbol.min.js"></script>
        <script src="assets/plugins/flot/jquery.flot.resize.min.js"></script>
        <script src="assets/plugins/flot/jquery.flot.tooltip.min.js"></script>
        <script src="assets/plugins/curvedlines/curvedLines.js"></script>
        <script src="assets/plugins/peity/jquery.peity.min.js"></script>
        <script src="assets/js/alpha.min.js"></script>
        
    <script>
        $(document).ready(function(){
       
            var name = '<?php echo $Username; ?>';
             setTimeout(function(){ Materialize.toast('Welcome '+name+'!', 4000) }, 4000);
             
             var flot1 = function () {
        var data = [<?php print_r($Sales);?>];
        var dataset = [{
            data: data,
            color: "#9575CD"
        }];
        var ticks = [<?php print_r($Names);?>];

        var options = {
            series: {
                bars: {
                    show: true,
                    fill: 1
                }
            },
            bars: {
                align: "center",
                barWidth: 0.3
            },
            xaxis: {
                ticks: ticks,
                tickLength: 0
            },
            legend: {
                show: false
            },
            grid: {
                color: "#AFAFAF",
                hoverable: true,
                borderWidth: 0,
                backgroundColor: '#FFF'
            },
            tooltip: true,
            tooltipOpts: {
                content: "X: %x, Y: %y",
                defaultTheme: false
            }
        };
        $.plot($("#FlotTk"), dataset, options);
    };

    flot1();
    
    var flot2 = function () {
        var data = [<?php print_r($Sales2);?>];
        var dataset = [{
            data: data,
            color: "#9575CD"
        }];
        var ticks = [<?php print_r($Prods);?>];

        var options = {
            series: {
                bars: {
                    show: true,
                    fill: 1
                }
            },
            bars: {
                align: "center",
                barWidth: 0.3
            },
            xaxis: {
                ticks: ticks,
                tickLength: 0
            },
            legend: {
                show: false
            },
            grid: {
                color: "#AFAFAF",
                hoverable: true,
                borderWidth: 0,
                backgroundColor: '#FFF'
            },
            tooltip: true,
            tooltipOpts: {
                content: "X: %x, Y: %y",
                defaultTheme: false
            }
        };
        $.plot($("#FlotTk2"), dataset, options);
    };

    flot2();
             
        });
    </script>
        
    </body>
</html>