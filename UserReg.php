<!DOCTYPE html>
<html lang="en">
    <head>
        
        <!-- Title -->
       <title>Axis Park | Create Admin</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />
        
        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="assets/plugins/materialize/css/materialize.min.css"/>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet"> 
       
        	
        <!-- Theme Styles -->
        <link href="assets/css/alpha.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>
        
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body>
        <div class="loader-bg"></div>
        <div class="loader">
            <div class="preloader-wrapper big active">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-spinner-teal lighten-1">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mn-content fixed-sidebar">
          
         
          
            <main class="mn-inner">
                <div class="row">
                    <div class="col s12">
                        <div class="page-title">SET UP USER ADMIN DETAILS</div>
                    </div>
                    <div class="col s12 m12 l6">
                        <div class="card">
                            <div class="card-content">
                                <span class="card-title">Input fields</span><br>
                                <div class="row">
                                    <form class="CreateUsers col s12" method="post" >
                                        <div class="row">
                                            <div class="input-field col s8">
                                                <input placeholder="Enter First Name" id="FirstName" name="FirstName" type="text" class="validate">
                                                <label for="CoName">First Name</label>
                                            </div>
                                        </div>
                                            <div class="row">
                                            <div class="input-field col s8">
                                                <input placeholder="Enter Last Name" id="LasName" name="LasName" type="text" class="validate">
                                                <label for="LasName">Last Name</label>
                                            </div>
                                        </div>
                                        
                                         <div class="row">
                                            <div class="input-field col s8">
                                                <input placeholder="Enter Username" id="Username" name="Username" type="text" class="validate">
                                                <label for="Username">Username</label>
                                            </div>
                                        </div>
                                        
                                         <div class="row">
                                            <div class="input-field col s8">
                                                <input placeholder="Enter Job Title" id="JobTitle" name="JobTitle" type="text" class="validate">
                                                <label for="JobTitle">Job Title</label>
                                            </div>
                                        </div>
                                        
                                         <div class="row">
                                            <div class="input-field col s8">
                                                <input placeholder="Enter Email" id="email" name="email" type="email" class="validate">
                                                <label for="email">Email</label>
                                            </div>
                                        </div>
                                        
                                         <div class="row">
                                            <div class="input-field col s8">
                                                <input placeholder="Enter Phone" id="phone" name="phone" type="text" class="validate">
                                                <label for="phone">Phone Number</label>
                                            </div>
                                        </div>
                                        
                                         <div class="row">
                                            <div class="input-field col s8">
                                                <input placeholder="Enter Password" id="passkey" name="passkey" type="password" class="validate">
                                                <label for="passkey">Password</label>
                                            </div>
                                        </div>
                                        
                                         <div class="row">
                                            <div class="input-field col s8">
                                                <input placeholder="Enter Password" id="ConfirmPasskey" name="Confirm" type="password" class="validate">
                                                <label for="passkey">Confirm Password</label>
                                            </div>
                                        </div>
                                        
                                        
                                         
                                        
                                        <div class ="row">
                                         <div class="col-sm-6 response">
                                              
                                                  </div>   
                                         <div class="col-sm-6">
                                                  <!-- <a  class="waves-effect waves-grey btn-flat"></a>-->
                                                  <a type="submit" name="btnUserDetails" class= "btnUserDetails waves-effect waves-light btn teal">Update Details</a>
                                                   </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                       
                      
                       
                      
                    </div>
                
                </div>
            </main>
          
        </div>
       
        
        <!-- Javascripts -->
        <script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
        <script src="assets/plugins/materialize/js/materialize.min.js"></script>
        <script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
        <script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
        <script src="assets/js/alpha.min.js"></script>
        <script src="assets/js/pages/form_elements.js"></script>
        
    </body>
</html>

<script>
    $(document).ready(function(){
        
         $(".btnUserDetails").hide();
          $(".btnUserDetails").prop("disabled",true);
        $("#ConfirmPasskey").on("change",function(){
            var InitPasskey = $("#passkey").val();
            if($(this).val()!==InitPasskey)
            {
                 $(".btnUserDetails").hide();
                $(".btnUserDetails").prop("disabled",true);
                alert("Passwords not matched!");
            }
            else{
                 $(".btnUserDetails").show("slow");
            }
        });
        
        $("#passkey").on("change",function(){
            var InitPasskey = $("#ConfirmPasskey").val();
            if($(this).val()!==InitPasskey)
            {
                 $(".btnUserDetails").hide();
                $(".btnUserDetails").prop("disabled",true);
                alert("Passwords not matched!");
            }
            else{
                 $(".btnUserDetails").show("slow");
            }
        });
        
        $(".btnUserDetails").click(function(ev){
            ev.preventDefault();
          $.post("engines/NewUser.php",$(".CreateUsers").serialize(),
          function(resp){
              console.log(resp);
              var fdbk = $.parseJSON(resp);
              if(fdbk.status=="ok")
              {
                  $(".response").html('<span class="new badge green">'+fdbk.msg+'</span>');
              }
              else{
                  $(".response").html('<span class="new badge red">'+fdbk.msg+'</span>');
              }
             
            });
            
        });
        
    });
    </script>

