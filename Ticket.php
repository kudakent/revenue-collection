<?php
require 'DB/DBAPI.php';
if (!Is_Logged_In()) {
redirect("login.php");
} else {
$UserData = UserDetails($_SESSION["acc"]);
$Username = $_SESSION["Username"];
$FirstName = $UserData[0]["UserFirstName"];
$LastName = $UserData[0]["UserSurname"];
$UserType = $UserData[0]["UserType"];
$InvNum = base64_decode($_GET["TktNum"]);

$basicInfo = getInvBasicInf($InvNum);
$InvDet = GetInvoiceDetails($InvNum);
$InvTender = get_invoice_tender($InvNum);
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- Title -->
        <title>Aximos | <?php echo basename($_SERVER['PHP_SELF']); ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />

        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="assets/plugins/materialize/css/materialize.min.css"/>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">        


        <!-- Theme Styles -->
        <link href="assets/css/alpha.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>

    </head>
    <body class="white">
        <?php //require 'config.php'; ?>

        <main class="mn-inner no-p">
            <div class="cyan darken-1">
              
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <div class="card card-transparent no-m">
                                <div class="card-content  white-text">
                                    <div class="row">
                                        <div class="col s12 m6 l6">
                                            <h4><?php echo $basicInfo[0]["CustomerName"]; ?></h4>

                                        </div>
                                        <div class="col s12 m6 l6 right-align">
                                            <h4>$<?php echo round($basicInfo[0]["InvoiceTotal"], 2); ?></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>

            <div class="container">
                <div class="row">
                 
                        <div class="card card-transparent no-m">
                            <div class="card-content invoice-relative-content">



                                <div class="row">
                                    <div class="col s6 m6 l3">
                                        <p>
                                            <span class="light-blue-text">Ticket Date</span><br>
                                            <b><?php echo date("M d, Y", strtotime($basicInfo[0]["CreatedDate"])); ?></b><br>
                                        </p>
                                    </div>
                                    <div class="col s12 m6 l6">
                                        <p>
                                            <span class="light-blue-text">Valid From</span><br>
                                            <b><?php echo date("M d, Y", strtotime($InvDet[0]["TimeIn"]))." @ ".date("H:i:s", strtotime($InvDet[0]["TimeIn"])); ?></b><br>
                                        </p>
                                    </div>
									
									<div class="col s6 m6 l3">
                                        <p>
                                            <span class="light-blue-text">Valid To</span><br>
                                            <b><?php echo date("M d, Y", strtotime($InvDet[0]["TimeOut"]))." @ ".date("H:i:s", strtotime($InvDet[0]["TimeOut"])); ?></b><br>
                                        </p>
                                    </div>
                                    <div class="col s12 m6 l6 right-align">
                                        <a class="btn-floating btn-large waves-effect waves-light light-green invoice-edit-btn" onclick="window.print()"><i class="material-icons">edit</i></a>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    
                                        <table class="table responsive-table">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Description</th>
                                                    <th class="right-align">Quantity</th>
                                                    <th class="right-align">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach($InvDet as $det){
                                                $prdct_name = $det['ProductName'];
                                                $qty = $det['ProductQty'];
                                                $unit_price = $det['ProductPrice'];
                                                $unit_VAT = $det['ProductVAT'];
                                                $total = $det['ProductTotal'];
                                               
                                                $arr = explode("#", $prdct_name, 2);
                                                $pdctCode = $arr[0];
                                              
?>
                                                <tr>
                                                <td><?php echo $pdctCode; ?></td>
                                                <td><?php echo $prdct_name; ?></td>
                                                <td class = "right-align"><?php echo $qty; ?></td>
                                                <td class = "right-align">$<?php echo $total; ?></td>
                                                </tr>
                                                <?php } 
                                                $TotalVat = $InvDet[0]["TotalVAT"];
                                                $InvTotal = $basicInfo[0]["InvoiceTotal"];
                                                $subtotal = $InvTotal-$TotalVat;
                                                ?>
                                                </tbody>
                                                </table>
                                            
                                                </div>
                                                
                                                </div>
                                                </div>
                                              
                </div>


                                                 <div class = "row">
                                                <div class = "col s6 m6 l6"><p>
                                                    <span class = "light-green-text"><b>Currency Summary</b></span></p>
                                                
                                                <table class="table responsive-table">
                                                    <thead>
                                                <tr>
                                                   
                                                    <th class="left-align">Currency</th>
                                                    <th class="left-align">Curr Equiv</th>
                                                    <th class="left-align">USD Equiv</th>
                                                </tr>
                                            </thead>
                                                    <tbody>
                                                        <?php 
                                                        foreach($InvTender as $Tendr){
                                                         $Currency = $Tendr["Currency"];
                                                         $Amount = $Tendr["AmountTendered"];
                                                         $USDEquiv = $Tendr["BaseCurrencyValue"];
                                                        ?>
                                                        <tr>
                                                            <td class="left-align"><?php echo $Currency; ?></td>
                                                    <td class="left-align"> <?php echo $Amount; ?></td>
                                                    <td class="left-align"> <?php echo $USDEquiv; ?></td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                    
                                                </table>
                                                
                                              </div>
                                                <div class = "col s12 m6 l6 right-align">
                                                <div class = "text-right">
                                                <h6 class = "m-t-sm light-blue-text"><b>Subtotal</b></h6>
                                                <h5 class = "">$<?php echo round($subtotal, 2); ?></h5>
                                                <div class = "divider"></div>
                                                <h6 class = "m-t-sm light-blue-text"><b>Total VAT</b></h6>
                                                <h5 class = "">$<?php echo round($TotalVat, 2); ?></h5>
                                                <div class = "divider"></div>
                                                <h6 class = "m-t-md text-success light-blue-text"><b>Total</b></h6>
                                                <h4 class = "text-success">$<?php echo round($InvTotal, 2); ?></h4>


                                                </div>
                                                </div>
                                                </div>
												
												
                                                </div>
                                                </main>

                                                
                                                <div class = "left-sidebar-hover"></div>

                                                <!--Javascripts -->
                                                <script src = "assets/plugins/jquery/jquery-2.2.0.min.js"></script>
                                            <script src="assets/plugins/materialize/js/materialize.min.js"></script>
                                            <script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
                                            <script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
                                            <script src="assets/js/alpha.min.js"></script>

                                            </body>
                                            </html>