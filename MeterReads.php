<?php
require 'DB/DBAPI.php';
if (!Is_Logged_In()) {
    redirect("login.php");
} else {
    $UserData = UserDetails($_SESSION["acc"]);
    $Username = $_SESSION["Username"];
    $FirstName = $UserData[0]["UserFirstName"];
    $LastName = $UserData[0]["UserSurname"];
    $UserType = $UserData[0]["UserType"];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- Title -->
        <title>Axis || <?php echo basename($_SERVER['PHP_SELF']); ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />

        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="assets/plugins/materialize/css/materialize.min.css"/>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">
        <link href="assets/plugins/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="assets/plugins/metrojs/MetroJs.min.css" rel="stylesheet">
        <link href="assets/plugins/weather-icons-master/css/weather-icons.min.css" rel="stylesheet">


        <!-- Theme Styles -->
        <link href="assets/css/alpha.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <?php require 'config.php'; ?>

        <main class="mn-inner">
          <div class="row EditDiv FocusDiv">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                           

                            <div class ="row">
                                <img id="imgpath" alt="No image" style="width:300px; height: 300px;"/>
                            </div>
                            <div class="row">
                                <div class="form-material col s5">
                                    <label for="addres">Address</label>
                                    <input id="addres" readonly="" name="addres" type="text" class="validate ">
                                    
                                </div>

                                <div class="form-material col s4">
                                    <label for="accnam">Account Number</label>
                                    <input class="form-control" id="accnam" readonly="" name="accnam" type="text" class="validate">
                                    
                                </div>
                                
                                <div class="form-material col s3">
                                    <label for="read">Recorded Reading</label>
                                    <input class="form-control" id="read" readonly="" name="read" type="text" class="validate">
                                    
                                </div>
                                
                                


                            </div>


                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                            <div class="row">
                                <div class="col s6 m6 l6">
                                    <span class="card-title">Readings Master</span>
                                </div>
                            </div>

                            <br>
                            <table id="example" class="display responsive-table datatable-example">
                                <thead>
                                    <tr>
                                        <th>Account Number</th>
                                        <th>Meter No</th>
                                        <th>Customer Name</th>
                                        <th>Reading</th>
                                        <th>Read Date</th>
                                        <th>Reader</th>
                                         <th>Actions</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    $MtrInfo = MeterReadings();
                                    foreach ($MtrInfo as $mtr) {
                                       $AccNum = $mtr["AccNum"];
                                       $MtrNo = $mtr["MeterNum"];
                                       $CustName = $mtr["CustomerName"];
                                       $Reading = $mtr["Reading"];
                                       $ReadDate = date("d M y", strtotime($mtr["ReadingDate"]));
                                       $Reader = $mtr["ReadBy"];
                                       $UserDetails = UserDetails($Reader);
                                       $Username = $UserDetails[0]["Username"];
                                       $addr = $mtr["Addr"];
                                       $imagePath = "pics/".$mtr["ImagePath"];
                                       ?>
                                    
                                    <tr id='<?php echo $AccNum; ?>'>
                                            <td><?php echo $AccNum; ?> </td>
                                            <td><?php echo $MtrNo; ?> </td>
                                            <td><?php echo $CustName; ?> </td>
                                            <td> <?php echo $Reading; ?></td>
                                            <td><?php echo $ReadDate; ?></td>
                                            <td><?php echo $Username; ?></td>
                                    <input type="hidden" read="<?php echo $Reading; ?>" acc="<?php echo $AccNum; ?>" imgpath ="<?php echo $imagePath; ?>" addr="<?php echo $addr; ?>"/> 
                                    <td>
                                        <a class="btn-floating btn-small waves-effect waves-light red viewImg" title="View Reading Image"><i class="small material-icons">visibility</i></a>
                                           </td>

                                    </tr> 

                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>

     
    <div class="left-sidebar-hover"></div>
    


    <!-- Javascripts -->
    <script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
    <script src="assets/plugins/materialize/js/materialize.min.js"></script>
    <script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
    <script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
    <script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/alpha.min.js"></script>
    <script src="assets/js/pages/table-data.js"></script>

    <script>
        $(document).ready(function () {

            $(".FocusDiv").hide();
            $(".EditDiv").hide();


            $('.btnFocusDiv').click(function () {
                $(".FocusDiv").show("slow");
                $(window).scrollTop($('.FocusDiv').offset().top - 40);
            });
                var id;
            $('.viewImg').click(function () {
                var tr = $(this).closest('tr');
                var acc = $(tr).find("input[type='hidden']").attr("acc");
                var imgpth = $(tr).find("input[type='hidden']").attr("imgpath");
                var addr = $(tr).find("input[type='hidden']").attr("addr");
                var read = $(tr).find("input[type='hidden']").attr("read");
              
                $("#accnam").val(acc);
                $("#addres").val(addr);
                $("#imgpath").attr("src",imgpth);
                 $("#read").val(read);
               
                $(".EditDiv").show("slow");
                $(window).scrollTop($('.FocusDiv').offset().top - 40);
            });



            var name = '<?php echo $Username; ?>';
            setTimeout(function () {
                Materialize.toast('Welcome ' + name + '!', 4000)
            }, 4000);



            $(".BtnCreateCurrency").click(function (ev) {
                ev.preventDefault();
                $.post("engines/CreateCurrency.php",
                        {
                            currency_name: $("#cudesc").val(),
                            ex_rate: $("#exrate").val(),
                            currency_code: $("#cucode").val()
                        },
                        function (response) {
                            var fdbk = $.parseJSON(response);
                            if (fdbk.status === "ok") {
                                alert(fdbk.msg);
                                var delay = 1000;
                                setTimeout(function () {
                                    location.reload();
                                }, delay);
                            } else {
                                alert(fdbk.msg);
                            }
                        });
            });
            
            $(".BtnEditCurrency").click(function(ev){
                ev.preventDefault();
                $.post("engines/EditCurrency.php",
                        {
                            currency_name: $("#editcudesc").val(),
                            ex_rate: $("#editexrate").val(),
                            currency_code: $("#editcucode").val(),
                            id:id
                        },
                        function (response) {
                            var fdbk = $.parseJSON(response);
                            if (fdbk.status === "ok") {
                                alert(fdbk.msg);
                                var delay = 1000;
                                setTimeout(function () {
                                    location.reload();
                                }, delay);
                            } else {
                                alert(fdbk.msg);
                            }
                        });
                });

        });
    </script>
</body>
</html>