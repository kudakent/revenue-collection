<?php
require 'DB/DBAPI.php';
if (!Is_Logged_In()) {
redirect("login.php");
} else {
$UserData = UserDetails($_SESSION["acc"]);
$Username = $_SESSION["Username"];
$FirstName = $UserData[0]["UserFirstName"];
$LastName = $UserData[0]["UserSurname"];
$UserType = $UserData[0]["UserType"];
$InvNum = base64_decode($_GET["TktNum"]);
$basicInfo = getInvBasicInf($InvNum);

$InvDet = GetInvoiceDetails($InvNum);
$Lat = $InvDet[0]["GPSLatitude"];
$Lon = $InvDet[0]["GPSLongitude"]; 
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        
        <!-- Title -->
        <title>Axis Park | <?php echo basename($_SERVER['PHP_SELF']); ?></title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />
        
        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="assets/plugins/materialize/css/materialize.min.css"/>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">        

        	
        <!-- Theme Styles -->
        <link href="assets/css/alpha.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>
        
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body>
    <?php require 'config.php'; ?>
            <main class="mn-inner" id="map-canvas">
            </main>
        </div>
        <div class="left-sidebar-hover"></div>
        
        <!-- Javascripts -->
        <script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
        <script src="assets/plugins/materialize/js/materialize.min.js"></script>
        <script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
        <script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>

        <script
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBXYnIsUWziNd4F48t63Nh9irV2sskLPhY&callback=initMap">
        </script>

        <script src="assets/js/alpha.min.js"></script>
        <script>
       
    $(document).ready(function(){
        
  
     var $mainCon    = jQuery('#main-container');
         $mainCon.css('position', 'relative');
      jQuery('#googleMap').css({
            'position': 'absolute',
              'top': $mainCon.css('padding-top'),
            'right': '0',
            'bottom': '0',
            'left': '0'
        });
    var lat = '<?php echo $Lat; ?>';
     var lon = '<?php echo $Lon; ?>';
     
var myCenter=new google.maps.LatLng(lat,lon);

function initialize()
{
var mapProp = {
  center:myCenter,
  zoom:15,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("map-canvas"),mapProp);

var marker=new google.maps.Marker({
  position:myCenter,
  title:"invoice location"

  });

marker.setMap(map);
var infowindow = new google.maps.InfoWindow({
  content:"Ticket Number <?php echo $InvNum; ?>"
  });

infowindow.open(map,marker);
}

google.maps.event.addDomListener(window, 'load', initialize);


  

});

</script>
        
    </body>
</html>