<?php


/* Login functions first */
//@session_start();
/* * *********************This is the main database API********It handles all database connection functions ****** */

//$db = new PDO("sqlsrv:Server=WIN-QPJF4N0OD9L;Database=AxiRevColl", "sa", "axis1234");
//$db = new PDO("sqlsrv:Server=PMUPFURUTSA;Database=AxiRevColl", "sa", "phillipkent8915#");
$db = new PDO("sqlsrv:Server=SQL5003.site4now.net;Database=DB_A33C8A_revcall", "DB_A33C8A_revcall_admin", "@x1551n01t@");
//$db = new PDO("sqlsrv:Server=AXIMOS-SERVER\MSSQLSERVER_AXMO;Database=AxiRevColl", "Axis", "Axis1234");



$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


//$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);

function get_today_sales(){
     global $db;
     //$result=array();
    try {
        $sql = $db->prepare('select sum(InvoiceTotal) as todaySales from tblInvoiceBasicInfo where CONVERT (DATE, CreatedDate)=? and InvoiceTotal>0');
        $sql->execute(array(date('Y-m-d')));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
       
   } catch (Exception $ex) {
       $result =  $ex->getMessage();
    }

    return $result;
}

function get_month_sales(){
     global $db;
     //$result=array();
    try {
       $this_month = date("m");
        $sql = $db->prepare('select sum(InvoiceTotal) from tblInvoiceBasicInfo where month(CreatedDate) =? and InvoiceTotal>0');
        $sql->execute(array($this_month));
        $result = $sql->fetchColumn();
       
   } catch (Exception $ex) {
       $result =  $ex->getMessage();
    }

    return $result;
}

function get_lasmon_sales(){
     global $db;
     //$result=array();
    try {
       $last_month = date('m', strtotime(date('Y-m')." -1 month"));
        $sql = $db->prepare('select sum(InvoiceTotal) from tblInvoiceBasicInfo where month(CreatedDate) =? and InvoiceTotal>0');
        $sql->execute(array($last_month));
        $result = $sql->fetchColumn();
       
   } catch (Exception $ex) {
       $result =  $ex->getMessage();
    }

    return $result;
}

function GetYearSalesByMarshal(){
    global $db;
     //$result=array();
    try {
       $ThisYear = date('Y');
        $sql = $db->prepare('select sum(InvoiceTotal) as InvoiceTotal,SalesManName from tblInvoiceBasicInfo where year(CreatedDate) =? and InvoiceTotal>0 group by SalesManName');
        $sql->execute(array($ThisYear));
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
       
   } catch (Exception $ex) {
       $result =  $ex->getMessage();
    }

    return $result; 
}

function GetYearSalesByService(){
    global $db;
     //$result=array();
    try {
       $ThisYear = date('Y');
        $sql = $db->prepare('select sum(ProductTotal) as InvoiceTotal,ProductName from tblInvoices where year(CreatedDate) =? and ProductTotal>0 group by ProductName');
        $sql->execute(array($ThisYear));
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
       
   } catch (Exception $ex) {
       $result =  $ex->getMessage();
    }

    return $result; 
}


//print_r(GetYearSalesByMarshal());

function get_invoice_coord(){
  global $db;
    try {

        $sql = $db->prepare('select distinct(InvoiceNum),GPSLatitude,GPSLongitude,InvoiceTotal,CustomerName from tblInvoices');
        $sql->execute();
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;   
}

function getRatePayerDistribution(){
  global $db;
    try {

        $sql = $db->prepare('select distinct(AccNum),MeterNum,Reading,ReadingDate,Latitude,Longitude,Note,CustomerName,Addr,Balance from vwCustomerMeterReadingMaster');
        $sql->execute();
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;   
}

function getCollectionCoords(){
  global $db;
    try {

        $sql = $db->prepare('select distinct(PaymentID),Latitude,longitude,Amount,CustomerName from tblRecievePayments');
        $sql->execute();
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;   
}


function getCollCoord($PaymentID){
  global $db;
    try {

        $sql = $db->prepare('select PaymentID,Latitude,longitude,Amount,CustomerName from tblRecievePayments where PaymentID = ? ');
        $sql->execute(array($PaymentID));
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;   
}

function get_invoiceCord($shiftnum){
  global $db;
    try {

        $sql = $db->prepare('select InvoiceNum,GPSLatitude,GPSLongitude,InvoiceTotal,CustomerName from tblInvoices where InvoiceNum IN (select InvoiceNum FROM tblInvoiceBasicInfo where ShiftRefence =?)');
        $sql->execute(array($shiftnum));
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;   
}

//Function for payments received
function getPaymentsCoords(){
    global $db;
      try {
  
          $sql = $db->prepare('select distinct(PaymentID),Latitude,longitude,Amount,CustomerName from tblRecievePayments');
          $sql->execute();
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }
  
  
  function getPaymentsCoord($PaymentID){
    global $db;
      try {
  
          $sql = $db->prepare('select PaymentID,Latitude,longitude,Amount,CustomerName from tblRecievePayments where PaymentID = ? ');
          $sql->execute(array($PaymentID));
          $result = $sql->fetchAll(PDO::FETCH_ASSOC);
      } catch (Exception $ex) {
          $result = $ex->getMessage();
      }
      return $result;   
  }