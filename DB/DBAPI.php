<?php

/* Login functions first */
session_start();
/* * *********************This is the main database API********It handles all database connection functions ****** */

//$db = new PDO("sqlsrv:Server=PMUPFURUTSA;Database=AxiRevColl", "sa", "phillipkent8915#");
$db = new PDO("sqlsrv:Server=SQL5003.site4now.net;Database=DB_A33C8A_revcall", "DB_A33C8A_revcall_admin", "@x1551n01t@");
//$db = new PDO("sqlsrv:Server=WIN-QPJF4N0OD9L;Database=AxiRevColl", "sa", "axis1234");
//$db = new PDO("sqlsrv:Server=AXIMOS-SERVER\MSSQLSERVER_AXMO;Database=AxiRevColl", "Axis", "Axis1234");
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


function redirect($url) {
    header("Location: $url");
}

function create_company_details($companyName, $CompanyAddr, $CompanyEmail, $CompanyBPN, $companyVAT, $companyLogo, $user_acc) {
    global $db;
    $result = array();
    try {
        $sql = $db->prepare('insert into tblCompanyDetails (CompanyName,CompanyAddress,CompanyEmail,CompanyBPN,CompanyVATN,CompanyLogo,DateSet,SetBy) values(?,?,?,?,?,?,?,?)');
        $sql->execute(array($companyName, $CompanyAddr, $CompanyEmail, $CompanyBPN, $companyVAT, $companyLogo, date('Y-m-d H:i:s'), $user_acc));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}

function edit_company_details($companyName, $CompanyAddr, $CompanyBPN, $companyVAT, $companyLogo) 
{
    global $db;
    $result = array();
    try {
        $sql = $db->prepare('update tblCompanyDetails set CompanyName=?,CompanyAddress=?,CompanyBPN=?,CompanyVATN=?,CompanyLogo=?,UpdatedDate=?,UpdatedBy=?');
        $sql->execute(array($companyName, $CompanyAddr, $CompanyBPN, $companyVAT, $companyLogo, date('Y-m-d H:i:s'), $_SESSION['acc']));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result["status"] = "ok";
        } else {
            $result["status"] = "fail";
        }
    } catch (Exception $ex) {
        $result["status"] = $ex->getMessage();
    }
    return $result;
}

function this_co_details() {

    global $db;
    try {

        $sql = $db->prepare(' select * from tblCompanyDetails');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

//user login
function AdminLogin($username, $password) {
    global $db;

    try {

        $sql = $db->prepare('select * from tblUsers where (Username=? or EmailAddress=?) and password=? and deleted=0');
        $sql->execute(array($username, $username, $password));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $_SESSION['acc'] = $result[0]['UserID'];
            $_SESSION['Username'] = $result[0]['Username'];
            $status['status'] = 'passed';
        } else {
            $status['status'] = 'failed';
        }
    } catch (Exception $ex) {
        $status['status'] = $ex->getMessage();
    }

    return $status;
}

/* * ***********************if user if logged in, the script will head straight to home page************* */

function Is_Logged_In() {
    if (isset($_SESSION['acc'])) {
        return true;
    }
}



//kill session for each users
function logout() {
    session_destroy();
    unset($_SESSION['acc']);
    return true;
}

//redirect function
//checks number of rows for users table during login
function get_Num_Of_Rows() {
    global $db;
    try {
        $sql = $db->prepare('select count(*) as num_of_rows from tblUsers');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
        $num_of_rows = $result[0]['num_of_rows'];
    } catch (Exception $ex) {
        $ex->getMessage();
    }

    return $num_of_rows;
}

/* * **********this function creates a user, the first on logger on deployment************* */

function Create_User($username, $password, $userfirstmame, $usersurname, $jobtitle, $emailadress, $user_group, $user_phone, $MarshalServices, $status, $created_by) {
    global $db;
    $result = array();
    try {
        $RouteName = 'Open';
        $sql = $db->prepare('insert into tblUsers(Username,Password,UserFirstName,UserSurname,RouteName,JobTitle,EmailAddress,UserType,UserPhoneNumber,MarshalServices,Deleted,CreatedDate,CreatedBy) values (?,?,?,?,?,?,?,?,?,?,?,?,?)');
        $sql->execute(array($username, $password, $userfirstmame, $usersurname, $RouteName, $jobtitle, $emailadress, $user_group, $user_phone, $MarshalServices, $status, date('Y-m-d H:i:s'), $created_by));
        $counter = $sql->rowCount();
        $lastInsertId = $db->lastInsertId();
        if ($counter > 0) {
            $result['status'] = 'ok';
            $result['id'] = $lastInsertId;
        } else {
            $result['status'] = 'failed';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;
}

function edit_user($username, $userfirstmame, $usersurname, $jobtitle, $emailadress, $user_group, $user_phone, $MarshalServices, $user_ID) {
    global $db;
    $result = array();
    try {
        $sql = $db->prepare('update tblUsers set Username=?,UserFirstName=?,UserSurname=?,JobTitle=?,EmailAddress=?,UserType=?,UserPhoneNumber=?,MarshalServices=?,UpdatedDate=?,UpdatedBy=? where UserID=?');
        $sql->execute(array($username, $userfirstmame, $usersurname, $jobtitle, $emailadress, $user_group, $user_phone, $MarshalServices, date('Y-m-d H:i:s'), $_SESSION['acc'], $user_ID));
        $counter = $sql->rowCount();
        if ($counter > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'failed';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;
}

function edit_user_pass($passkey, $user_ID) {
    global $db;
    $result = array();
    try {
        $sql = $db->prepare('update tblUsers set Password=?,UpdatedDate=?,UpdatedBy=? where UserID=?');
        $sql->execute(array($passkey, date('Y-m-d H:i:s'), $_SESSION['acc'], $user_ID));
        $counter = $sql->rowCount();
        if ($counter > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'failed';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;
}

function UserDetails($id) {
    global $db;
    //$result=array();
    try {
        $sql = $db->prepare('select * from tblUsers where deleted=0 and UserID=?');
        $sql->execute(array($id));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

//get logged in user details
function get_all_users() {
    global $db;
    //$result=array();
    try {
        $sql = $db->prepare('select * from tblUsers where deleted=0 ');
        $sql->execute(array($_SESSION['acc']));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

//get all general users except administrators
function get_all_marshal_users() {
    global $db;
    //$result=array();
    try {
        $sql = $db->prepare('select * from tblUsers where deleted=0 and UserType!=?');
        $sql->execute(array("Administrator"));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}
//print_r(get_all_marshal_users());

function get_all_users_expt_logged() {
    global $db;
    //$result=array();
    try {
        $sql = $db->prepare('select * from tblUsers where deleted=0 and UserID!=?');
        $sql->execute(array($_SESSION['acc']));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function get_all_users_expt_this($UserID) {
    global $db;
    //$result=array();
    try {
        $sql = $db->prepare('select * from tblUsers where deleted=0 and UserID!=?');
        $sql->execute(array($UserID));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function gen_uuid() { //GENERATES uuid for each customer/customer table row for reference purposes
    return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,
            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );
}

function get_emails() {
    global $db;
    try {
        $sql = $db->prepare('select EmailAddress from tblUsers where deleted=0');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $ex->getMessage();
    }

    return $result;
}

//this function creates a new product
function create_product($ParentCode, $code, $desc, $uom, $excUntPri, $untPri, $TaxCode) {
    global $db;

    try {
        $sql = $db->prepare('insert into tblProducts(ParentCode,Code,Description,UOM,ExclUnitPrice,UnitPrice,TaxCode,Deleted,CreatedDate,CreatedBy) values (?,?,?,?,?,?,?,?,?,?)');
        $sql->execute(array($ParentCode, $code, $desc, $uom, $excUntPri, $untPri, $TaxCode, 0, date("Y-m-d H:i:s"), $_SESSION['acc']));
        $counter = $sql->rowCount();
        $lastID = $db->lastInsertId();
        if ($counter > 0) {
            $result['status'] = 'ok';
            $result['id'] = $lastID;
        } else {
            $result['status'] = 'failed';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;
}

function EditProduct($ParentCode,$code, $desc, $uom, $excUntPri, $untPri, $TaxCode, $PRODid) {
    global $db;

    try {
        $sql = $db->prepare('update tblProducts set ParentCode=?,Code=?,Description=?,UOM=?,ExclUnitPrice=?,UnitPrice=?,TaxCode=?,UpdatedDate=?,UpdatedBy=? where ProductID=?');
        $sql->execute(array($ParentCode,$code, $desc, $uom, $excUntPri, $untPri, $TaxCode, date("Y-m-d H:i:s"), $_SESSION['acc'], $PRODid));
        $counter = $sql->rowCount();
        if ($counter > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'failed';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;
}

function show_all_stocks() {
    global $db;
    //$result=array();
    try {
        $sql = $db->prepare('select * from tblProducts where deleted=0');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function ShowAllProdCodes() {
    global $db;
    //$result=array();
    try {
        $sql = $db->prepare('select Code as Codes from tblProducts where deleted=0');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function ShowServiceData($ProdID) {
    global $db;
    try {
        $sql = $db->prepare('select * from tblProducts where deleted=0 and [ProductID]=?');
        $sql->execute(array($ProdID));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function ShowCategories() {
    global $db;
    //$result=array();
    try {
        $sql = $db->prepare('select * from luProductCategories');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function get_Prod_name_code($code, $pname) {
    global $db;
    //$result=array();
    try {
        $sql = $db->prepare('select Code from tblProducts where deleted=0 and (Code=? or Description=?) ');
        $sql->execute(array($code, $pname));
        $output = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $result['status'] = 'exist';
        } else {
            $result['status'] = 'doesNot';
        }
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function GetProdCodeExcl($code) {
    global $db;
    //$result=array();
    try {
        $sql = $db->prepare('select Code from tblProducts where deleted=0 and Code!=? ');
        $sql->execute(array($code));
        $output = $sql->fetchALL(PDO::FETCH_ASSOC);
        if ($sql->rowCount() > 0) {
            $result['status'] = 'exist';
        } else {
            $result['status'] = 'doesNot';
        }
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

// creTE Shift for ticket selling

function CreateShift($MarshalID, $Marshal, $AreaLabel,$ShiftType) {
    global $db;
    $result = array();
    $status = 'Created';
    try {
        $sql = $db->prepare('insert into tblDailyShifts(MarshalID,Marshal,AssignedArea,ShiftType,ShiftStatus,CreatedDate,CreatedBy) values (?,?,?,?,?,?,?)');
        $sql->execute(array($MarshalID, $Marshal, $AreaLabel,$ShiftType, $status, date("Y-m-d H:i:s"), $_SESSION['acc']));
        $counter = $sql->rowCount();
        if ($counter > 0) {
            $lastinsertID = $db->lastInsertId();
            $sheetNumber = sprintf('%06d', $lastinsertID);
            $this_sql = $db->prepare('update tblDailyShifts set ShiftNumber=? where DailyShiftID=?');
            $this_sql->execute(array($sheetNumber, $lastinsertID));
            $this_counter = $this_sql->rowCount();
            if ($this_counter > 0) {
                $result['status'] = 'ok';
                $result['id'] = $lastinsertID;
                $result['sheetNumber'] = $sheetNumber;
            } else {
                $result['status'] = 'inside_fail';
            }
        } else {
            $result['status'] = 'failed';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;
}

function ShowShifts() {
    global $db;
    try {
        $sql = $db->prepare('select * from tblDailyShifts');
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function ShowStartedShifts() {
    global $db;
    try {
        $shiftState = "Created";
        $sql = $db->prepare('select * from tblDailyShifts where ShiftStatus = ?');
        $sql->execute(array($shiftState));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function GetMarshals() {
    global $db;
    try {
        $sql = $db->prepare("select * from tblUsers where Deleted=0 and UserType='Marshal'");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function CreateSlot($PointA, $PointB, $PointAlong, $AreaLabel) {
    global $db;

    try {
        $sql = $db->prepare('insert into TblParkingSlots(PointA,PointB,PointAlong,AreaLabel,DateCreated,CreatedBy) values (?,?,?,?,?,?)');
        $sql->execute(array($PointA, $PointB, $PointAlong, $AreaLabel, date("Y-m-d H:i:s"), $_SESSION['acc']));
        $counter = $sql->rowCount();
        if ($counter > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'failed';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;
}

function GetSlots() {
    global $db;
    try {
        $sql = $db->prepare("select * from TblParkingSlots");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function GetSlotsSales($AreaLabel) {
    global $db;
    try {
        $sql = $db->prepare("select sum(InvoiceTotal) as Sls from tblInvoiceBasicInfo where ShiftRefence in (select ShiftNumber from tblDailyShifts where AssignedArea = ?)");
        $sql->execute(array($AreaLabel));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function GetReceipts() {
    global $db;
    try {
        $sql = $db->prepare("select * from tblInvoiceBasicInfo");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function GetReceiptsToSync() {
    global $db;
    try {
        $sql = $db->prepare("select * from tblInvoiceBasicInfo where InvoiceNum IN (select InvoiceNum from tblInvoices where ErpSync = 0)");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function GetActReceiptsToSync() {
    global $db;
    try {
        $sql = $db->prepare("select * from tblInvoices where ErpSync = 0");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function getIncomeCodeDet($SvcCode){
  global $db;
    try {
        $sql = $db->prepare("select * from [vwIncToServices] where Code = ?");
        $sql->execute(array($SvcCode));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;  
}

function GetShiftReceipts($shft) {
    global $db;
    try {
        $sql = $db->prepare("select * from tblInvoiceBasicInfo where ShiftRefence=?");
        $sql->execute(array($shft));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function UpdateShiftStatus($status, $sheetNumber) {
    global $db;
    try {
        $sql = $db->prepare('update tblDailyShifts set ShiftStatus=?,UpdatedDate=?,UpdatedBy=? where ShiftNumber=?');
        $sql->execute(array($status, date("Y-m-d H:i:s"), $_SESSION['acc'], $sheetNumber));
        $counter = $sql->rowCount();
        if ($counter > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'failed';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;
}

function CancellShift($status, $cance_reason, $ShftNum) {
    global $db;
    try {

        $sql = $db->prepare('update tblDailyShifts set ShiftStatus=?, CancellationReason=?, CancelledBy=?,CancelledDate=? where ShiftNumber=?');
        $sql->execute(array($status, $cance_reason, $_SESSION['acc'], date('Y-m-d H:i:s'), $ShftNum));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'notok';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;
}

function getInvBasicInf($InvNum) {
    global $db;
    try {
        $sql = $db->prepare("select * from tblInvoiceBasicInfo where InvoiceNum=?");
        $sql->execute(array($InvNum));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function GetInvoiceDetails($InvNum) {
    global $db;
    try {
        $sql = $db->prepare("select * from tblInvoices where InvoiceNum=?");
        $sql->execute(array($InvNum));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function GetPayments() {
    global $db;
    try {
        $sql = $db->prepare("select * from tblRecievePayments where ReasonsPaidFor <> 'Billing'");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function GetCustomerCollections() {
    global $db;
    try {
        $sql = $db->prepare("select * from tblRecievePayments where ReasonsPaidFor = 'Billing'");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

//print_r(GetCustomerCollections());

function GetCollectionToSync() {
    global $db;
    try {
        $sql = $db->prepare("select * from tblRecievePayments where ReasonsPaidFor = 'Billing' and Status = 'Unused'");
        $sql->execute();
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function UpdateAccSyncStatus($status,$PaymentID){
     global $db;
 
    try{
        $stm =  $db->prepare("update tblRecievePayments set Status=?,SyncDate = ?,SyncBy = ? where PaymentID=?");
        $stm->execute(array($status,date("Y-m-d H:i:s"),$_SESSION["acc"], $PaymentID));
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        
    } catch (Exception $ex) {
               $result=$ex->getMessage();
    }
    
    return $result;
}

function UpdateRecSyncStatus($status,$InvID){
     global $db;
 
    try{
        $stm =  $db->prepare("update tblInvoices set ErpSync=?,SyncDate = ?,SyncBy = ? where InvoiceID=?");
        $stm->execute(array($status,date("Y-m-d H:i:s"),$_SESSION["acc"], $InvID));
        $result = $stm->fetchAll(PDO::FETCH_ASSOC);
        
    } catch (Exception $ex) {
               $result=$ex->getMessage();
    }
    
    return $result;
}

function GetPaymentWithId($Pid) {
    global $db;
    try {
        $sql = $db->prepare("select * from tblRecievePayments where PaymentID=?");
        $sql->execute(array($Pid));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function GetOutstandingSales($CarReg) {

    global $db;
    try {
        $sql = $db->prepare("select * from TblViewOwings where CustomerName=? order by Timein asc");
        $sql->execute(array($CarReg));
        $result = $sql->fetchALL(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }

    return $result;
}

function UpdatePayment($BalanceUnused, $status, $UserLogged, $PaymentID) {
    global $db;
    try {
        $sql = $db->prepare('update tblRecievePayments set BalanceUnused=?, Status=?, DateUsed=?,UsedBy=? where PaymentID=?');
        $sql->execute(array($BalanceUnused, $status, Date("Y-m-d H:i:s"), $UserLogged, $PaymentID));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'notok';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;
}

/* print_r(UpdatePayment(1,"Unused","",1));
  die(); */

Function UpdateInvoiceStatus($OutstandingBalance, $InvStatus, $InvoiceNum) {
    global $db;
    try {

        $sql = $db->prepare('update tblInvoiceBasicInfo set OutstandingBalance=?, InvoiceStatus=?  where InvoiceNum=?');
        $sql->execute(array($OutstandingBalance, $InvStatus, $InvoiceNum));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'notok';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;
}

function create_currency_exchange_rates($currency, $CurrencyCode, $exchangerate) {
    global $db;
    try {

        $sql = $db->prepare('insert into tblCurrency(Currency,CurrencyCode,USDExchangeRate,DateSet,SetBy,status) values (?,?,?,?,?,?)');
        $sql->execute(array($currency, $CurrencyCode, $exchangerate, date('Y-m-d H:i:s'), $_SESSION['acc'], 'active'));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'fail';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;
}

function get_tender_types() {
    global $db;
    try {

        $sql = $db->prepare("select * from tblCurrency where status='active'");
        $sql->execute();
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}

function GetCurrency($Code) {
    global $db;
    try {

        $sql = $db->prepare("select * from tblCurrency where status='active' and CurrencyCode=?");
        $sql->execute(array($Code));
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}

function update_exchage_rate($currency, $currency_code, $exchangerate, $ExchangeRateID) {
    global $db;
    try {

        $sql = $db->prepare('update tblCurrency set Currency=?,CurrencyCode=?,USDExchangeRate=?,UpdatedDate=?,UpdatedBy=? where ExchangeRateID=?');
        $sql->execute(array($currency, $currency_code, $exchangerate, date('Y-m-d H:i:s'), $_SESSION['acc'], $ExchangeRateID));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'fail';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;
}

function delete_currency($ExchangeRateID) {
    global $db;
    try {

        $sql = $db->prepare("update tblCurrency set status='inactive',UpdatedDate=?,UpdatedBy=? where ExchangeRateID=?");
        $sql->execute(array(date('Y-m-d H:i:s'), $_SESSION['acc'], $ExchangeRateID));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'fail';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;
}

function get_invoice_tender($invNumber) {
    global $db;
    try {

        $sql = $db->prepare('select * from tblInvoiceTender where InvNumber=?');
        $sql->execute(array($invNumber));
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}

function GetIncCodes() {
    global $db;
    try {

        $sql = $db->prepare('select * from tblIncCodeMaster');
        $sql->execute();
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}

function CreateIncCodes($IncCode, $IncDescription, $IncVat, $GLAlloc) {
    global $db;
    try {

        $sql = $db->prepare('insert into tblIncCodeMaster(IncCode,IncDescription,IncVat,GLAlloc,DateSynced,SyncedBy) values (?,?,?,?,?,?)');
        $sql->execute(array($IncCode, $IncDescription, $IncVat, $GLAlloc, date('Y-m-d H:i:s'), $_SESSION['acc']));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'fail';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;
}

function DeleteIncCodes() {
    global $db;
    try {

        $stmt = $db->prepare("delete from tblIncCodeMaster");
        $stmt->execute();

        $reset_ID = $db->prepare("DBCC CHECKIDENT (tblIncCodeMaster,RESEED,0)");
        $reset_ID->execute();

        $result = "done";
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}

function GetCustomers(){
  global $db;
    try {

        $sql = $db->prepare('select * from tblCustomers');
        $sql->execute();
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;  
}

function GetMeterCustomers(){
  global $db;
    try {

        $sql = $db->prepare('select * from luMeterHeader');
        $sql->execute();
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;  
}

function DeleteCustomers() {
    global $db;
    try {

        $stmt = $db->prepare("delete from tblCustomers");
        $stmt->execute();

        $reset_ID = $db->prepare("DBCC CHECKIDENT (tblCustomers,RESEED,0)");
        $reset_ID->execute();

        $result = "done";
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}

function DeleteCusMeters() {
    global $db;
    try {

        $stmt = $db->prepare("delete from luMeterHeader");
        $stmt->execute();

        $reset_ID = $db->prepare("DBCC CHECKIDENT (luMeterHeader,RESEED,0)");
        $reset_ID->execute();

        $result = "done";
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}

function DeleteNotes() {
    global $db;
    try {

        $stmt = $db->prepare("delete from tblNotesMaster");
        $stmt->execute();

        $reset_ID = $db->prepare("DBCC CHECKIDENT (tblNotesMaster,RESEED,0)");
        $reset_ID->execute();

        $result = "done";
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;
}

function CreateCustomer($CustomerNumber,$CustomerName,$Addr,$Phone,$LastPayDate,$LastPayAmnt,$Balance) {
    global $db;
    try {

        $sql = $db->prepare('insert into tblCustomers(CustomerNumber,CustomerName,Addr,Phone,LastPayDate,LastPayAmnt,Balance,UpdatedDate,UpdatedBy) values (?,?,?,?,?,?,?,?,?)');
        $sql->execute(array($CustomerNumber,$CustomerName,$Addr,$Phone,$LastPayDate,$LastPayAmnt,$Balance, date('Y-m-d H:i:s'), $_SESSION['acc']));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'fail';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;
}

function UpdateMeterData($meternum,$routename,$acc) 
        {
    global $db;
    try {

        $sql = $db->prepare('update tblCustomers set MeterNum = ? ,RouteID=? where CustomerNumber = ?');
        $sql->execute(array($meternum,$routename,$acc));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'fail';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;
}

function CreateMeterData($MeterNo,$RouteNo,$RouteID,$RouteName,$Acc,$Name,$Address,$Ward,$Suburb,$LastRead,$LastReadDate){
   global $db;
    try {
        $sql = $db->prepare('insert into luMeterHeader (MeterNo,RouteNo,RouteID,RouteName,Acc,Name,Address,Ward,Suburb,LastReading,lastreaddate,LastSyncDate) values(?,?,?,?,?,?,?,?,?,?,?,?)');
        $sql->execute(array($MeterNo,$RouteNo,$RouteID,$RouteName,$Acc,$Name,$Address,$Ward,$Suburb,$LastRead,$LastReadDate,date("Y-m-d H:i:s")));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'fail';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;  
}



function GetAccountData($Acc){
   global $db;
    try {

        $sql = $db->prepare('select * from tblCustomers where CustomerNumber = ?');
        $sql->execute(array($Acc));
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;   
}

function ResetPassword($UserID)
{
   global $db;

    try {
        $password = "00000";
        $password = md5($password);
        $sql = $db->prepare('update tblUsers set Password=?,UpdatedDate=?,UpdatedBy=? where UserID=?');
        $sql->execute(array($password,date("Y-m-d H:i:s"),$_SESSION["acc"],$UserID));
        $counter = $sql->rowCount();
        if ($counter > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'failed';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;  
}

function CreateHeader($McNo,$Cash,$RecStatus,$RecDate,$status){
    global $db;
    try {

        $sql = $db->prepare('insert into tblMunrctctr(McNo,Cash,RecStatus,RecDate,DateCreated,CreatedBy,Status) values (?,?,?,?,?,?,?)');
        $sql->execute(array($McNo,$Cash,$RecStatus,$RecDate, date('Y-m-d H:i:s'), $_SESSION['acc'],$status));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'fail';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;
}

function getMunrctctr(){
     global $db;
    try {

        $sql = $db->prepare('select * from tblMunrctctr');
        $sql->execute();
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;  
}

function CreateNoteHeader($NoteCode,$NoteDesc){
   global $db;
    try {

        $sql = $db->prepare('insert into tblNotesMaster(NoteCode,NoteDesc,DateCreated,CreatedBy) values (?,?,?,?)');
        $sql->execute(array($NoteCode,$NoteDesc, date('Y-m-d H:i:s'), $_SESSION['acc']));
        $count = $sql->rowCount();
        if ($count > 0) {
            $result['status'] = 'ok';
        } else {
            $result['status'] = 'fail';
        }
    } catch (Exception $ex) {
        $result['status'] = $ex->getMessage();
    }
    return $result;  
}

function getNotes()
{
   global $db;
    try {

        $sql = $db->prepare('select * from tblNotesMaster');
        $sql->execute();
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;   
}

function MeterReadings(){
  global $db;
    try {

        $sql = $db->prepare('select * from vwCustomerMeterReadingMaster');
        $sql->execute();
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;   
}

function getDistinctRoutes()
{
    global $db;
    try {

        $sql = $db->prepare('SELECT  RouteNo,RouteID,RouteName
  FROM (SELECT RouteNo,RouteID,RouteName, ROW_NUMBER() over (PARTITION BY RouteID order by RouteName)
      AS val FROM luMeterHeader) f WHERE val = 1 and RouteName is not NULL');
        $sql->execute();
        $result = $sql->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $ex) {
        $result = $ex->getMessage();
    }
    return $result;    
}

