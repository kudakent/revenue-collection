<!DOCTYPE html>

<html lang="en">
    <head>
        
        <!-- Title -->
       <title>Axis Park | Company details</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />
        
        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="assets/plugins/materialize/css/materialize.min.css"/>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet"> 
       
        	
        <!-- Theme Styles -->
        <link href="assets/css/alpha.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>
        
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body>
        <div class="loader-bg"></div>
        <div class="loader">
            <div class="preloader-wrapper big active">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-spinner-teal lighten-1">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mn-content fixed-sidebar">
          
         
          
            <main class="mn-inner">
                <div class="row">
                    <div class="col s12">
                        <div class="page-title">SET UP DETAILS</div>
                    </div>
                    <div class="col s12 m12 l6">
                        <div class="card">
                            <div class="card-content">
                                <span class="card-title">Input fields</span><br>
                                <div class="row">
                                    <form class="CreateCo col s12" method="post" >
                                        <div class="row">
                                            <div class="input-field col s6">
                                                <input placeholder="Enter Company Name" id="CoName" name="CoName" type="text" class="validate">
                                                <label for="CoName">Company Name</label>
                                            </div>
                                            <div class="input-field col s6">
                                                <input placeholder="Enter email" id="email" name="email" type="email" class="validate">
                                                <label for="Vat">Company Email</label>
                                            </div>
                                        </div>
                                         <div class="row">
                                            <div class="input-field col s6">
                                                <input placeholder="Enter BPN" id="BPN" name="BPN" type="text" class="validate">
                                                <label for="BPN">Company BPN</label>
                                            </div>
                                             
                                             <div class="input-field col s6">
                                                <input placeholder="Enter VAT" id="Vat" name="Vat" type="text" class="validate">
                                                <label for="Vat">Company VAT</label>
                                            </div>
                                          
                                        </div>
                                         <div class="row">
                                            <div class="input-field col s12">
                                                <input placeholder="Enter address, using comma to split address componets" id="Address" name="Address" type="text" class="validate">
                                                <label for="Address">Company Address</label>
                                            </div>
                                           
                                        </div>
                                        
                                        <div class ="row">
                                         <div class="col-sm-6 response">
                                                  </div>   
                                         <div class="col-sm-6">
                                                  <!-- <a  class="waves-effect waves-grey btn-flat"></a>-->
                                                  <a type="submit" name="loginbutton" class= "BtnUpdate waves-effect waves-light btn teal">Update Details</a>
                                                   </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                       
                      
                       
                      
                    </div>
                
                </div>
            </main>
          
        </div>
       
        
        <!-- Javascripts -->
        <script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
        <script src="assets/plugins/materialize/js/materialize.min.js"></script>
        <script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
        <script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
        <script src="assets/js/alpha.min.js"></script>
        <script src="assets/js/pages/form_elements.js"></script>
        
    </body>
</html>

<script>
    $(document).ready(function(){
        
        $(".BtnUpdate").click(function(ev){
            ev.preventDefault();
            var Id = "<?php echo $_GET["acc"]; ?>";
          $.post("engines/CoReg.php",$(".CreateCo").serialize(),
          function(resp){
              console.log(resp);
              var fdbk = $.parseJSON(resp);
              if(fdbk.status==="ok")
              {
                  $(".response").html('<div class="green-text">'+fdbk.msg+'</div>');
                  var delay = 1000; 
                    setTimeout(function(){ window.location = "index.php?acc="+Id; }, delay);
              }
              else{
                  $(".response").html('<div class="red-text">'+fdbk.msg+'</div>');
              }
             
            });
            
        });
        
    });
    </script>

