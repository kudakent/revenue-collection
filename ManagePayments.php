<?php
require 'DB/DBAPI.php';
if (!Is_Logged_In()) {
    redirect("login.php");
} else {
    $UserData = UserDetails($_SESSION["acc"]);
    $Username = $_SESSION["Username"];
    $FirstName = $UserData[0]["UserFirstName"];
    $LastName = $UserData[0]["UserSurname"];
    $UserType = $UserData[0]["UserType"];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- Title -->
        <title>Axis Park | <?php echo basename($_SERVER['PHP_SELF']); ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />

        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="assets/plugins/materialize/css/materialize.min.css"/>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">
        <link href="assets/plugins/datatables/css/jquery.dataTables.min.css" rel="stylesheet">


        <!-- Theme Styles -->
        <link href="assets/css/alpha.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <?php require 'config.php'; ?>

        <main class="mn-inner">

            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                            <div class="row">
                                <div class="col s6 m6 l6">
                                    <span class="card-title">Payments Summary</span><br>
                                </div>
                            </div>

                            <br>
                            <table id="example" class="display responsive-table datatable-example">
                                <thead>
                                    <tr>
                                        <th>Reference</th>
                                        <th>Amount</th>
                                        <th>Marshal</th>
                                        <th>Status</th>
                                        <th>Date Created</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    $Payments = GetPayments();
                                    foreach ($Payments as $Pay) {
                                        $PaymentID = $Pay["PaymentID"];
                                        $CarReg = $Pay["CustomerName"];
                                        $Marshal = $Pay["SalesRep"];
                                        $CreatedDate = date("d M y", strtotime($Pay["CreatedDate"]));
                                        $Status = $Pay["Status"];
                                        $Amnt = $Pay["Amount"];
                                        if ($Status != "Used") {
                                            $Status = '<div class="chip blue">Not Used</div>';
                                        } else {
                                            $Status = '<div class="chip red">Used</div>';
                                        }
                                        ?>
                                        <tr id="<?php echo $CarReg; ?>">
                                            <td><?php echo $CarReg; ?> </td>
                                            <td><?php echo $Amnt; ?></td>
                                            <td> <?php echo $Marshal; ?></td>
                                            <td> <?php echo $Status; ?> </td>
                                            <td><?php echo $CreatedDate; ?></td>
                                            <td>
                                                <?php if ($Pay["Status"] != "Used") { ?> 
                                                    <a class="btn-floating btn-small waves-effect waves-light blue " href="ViewCarReceipts?Pid=<?php echo $PaymentID; ?>&Val=<?php echo base64_encode($Amnt); ?>" title="Effect Payments"><i class="small material-icons">visibility</i></a>
                                                <?php } ?>
                                                <a class="btn-floating waves-effect waves-light red" href="PaymentLoc?PayID=<?php echo  base64_encode($PaymentID); ?>" title="View Payment Location"><i class="small material-icons">my_location</i></a>
                                            </td>
                                        </tr> 

                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>

    </div>
    <div class="left-sidebar-hover"></div>

  
    <!-- Javascripts -->
    <script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
    
    <script src="assets/plugins/materialize/js/materialize.min.js"></script>
    <script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
    <script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
    <script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/alpha.min.js"></script>
    <script src="assets/js/pages/table-data.js"></script>

    <script>
        $(document).ready(function () {



            $(".FocusDiv").hide();

            $('.btnFocusDiv').click(function () {
                $(".FocusDiv").show("slow");
                $(window).scrollTop($('.FocusDiv').offset().top - 40);
            });


            var name = '<?php echo $Username; ?>';
            setTimeout(function () {
                Materialize.toast('Welcome ' + name + '!', 4000)
            }, 4000);

            var shftNum;
            $(".cancelShift").click(function (ev) {
                ev.preventDefault();
                shftNum = $(this).closest('tr').attr('id');
                window.location.href = "CancelShift.php?ShftNum=" + shftNum;
            });

            $(".BtnCreateShift").click(function (ev) {
                ev.preventDefault();
                $.post("engines/CreateShift.php",
                        {
                            marshals: $("#marshals").val(),
                            AreaLabel: $("#parking_area").val()
                        },
                        function (response) {
                            var fdbk = $.parseJSON(response);
                            if (fdbk.status === "ok") {
                                alert(fdbk.msg);
                                var delay = 1000;
                                setTimeout(function () {
                                    location.reload();
                                }, delay);
                            } else {
                                alert(fdbk.msg);
                            }
                        });
            });
            
          

        });
    </script>
</body>
</html>