<?php
require 'DB/DBAPI.php';
if (!Is_Logged_In()) {
    redirect("login.php");
} else {
    $UserData = UserDetails($_SESSION["acc"]);
    $Username = $_SESSION["Username"];
    $FirstName = $UserData[0]["UserFirstName"];
    $LastName = $UserData[0]["UserSurname"];
    $UserType = $UserData[0]["UserType"];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- Title -->
        <title>Axis Park | <?php echo basename($_SERVER['PHP_SELF']); ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />

        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="assets/plugins/materialize/css/materialize.min.css"/>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">
        <link href="assets/plugins/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="assets/plugins/weather-icons-master/css/weather-icons.min.css" rel="stylesheet">


        <!-- Theme Styles -->
        <link href="assets/css/alpha.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <?php require 'config.php'; ?>

        <main class="mn-inner">
            <div class="row FocusDiv">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                            <h5>New Shift</h5>


                            <div class="row">
                                <div class="input-field col s5">
                                    <select id="marshals" name="marshals"  tabindex="-1" >
                                        <?php
                                        $Marshals = GetMarshals();
                                        foreach ($Marshals as $Marsh) {
                                            ?>
                                            <option value="<?php echo $Marsh["UserID"] . ',' . $Marsh["Username"]; ?>"><?php echo $Marsh["Username"]; ?></option>
                                        <?php } ?>
                                    </select>
                                    <label>Select Marshal</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s5">
                                    <select id="shift_type" name="shift_type"  tabindex="-1" >

                                        <option value="NB">Vending</option>
                                        <option value="B">Collections</option>
                                        <option value="WM">Water Meter Reading</option>

                                    </select>
                                    <label>Shift Type</label>
                                </div>
                            </div>

                            <div class="row area">
                                <div class="input-field col s5">
                                    <select id="parking_area" name="parking_area"  tabindex="-1" >
                                        <?php
                                        $PAreas = GetSlots();
                                        foreach ($PAreas as $areas) {
                                            ?>
                                            <option value="<?php echo $areas["AreaLabel"]; ?>"><?php echo $areas["AreaLabel"]; ?></option>
                                        <?php } ?>
                                    </select>
                                    <label>Select Area</label>
                                </div>
                            </div>

                             <div class="row routes">
                                <div class="input-field col s5">
                                    <select class="form-control myroutes" id="mega-s"  name="routes[]"   multiple="" >
                                        <option value="">Choose Route</option>
                                        <?php
                                        $Routes = getDistinctRoutes();
                                        foreach ($Routes as $route) {
                                            ?>
                                            <option value="<?php echo $route["RouteID"]; ?>"><?php echo $route["RouteName"]." (".$route["RouteID"].")"; ?></option>
                                        <?php } ?>
                                    </select>
                                    <label>Select Routes</label>
                                </div>
                            </div>

                            <div class ="row">

                                <div class="col s4 l4 m4">
                                    <!-- <a  class="waves-effect waves-grey btn-flat"></a>-->
                                    <a type="submit" name="BtnCreateShift" class= "BtnCreateShift waves-effect waves-light btn blue m-b-xs">Create Shift</a>
                                </div>


                            </div>


                        </div>
                    </div>
                </div>

            </div>

            <div class="row cancelDiv">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                            <h5>Cancel Shift</h5>


                            <div class="row">
                                <div class="input-field col s5">
                                    <select id="shifts" name="shifts"  tabindex="-1" >
                                        <?php
                                        $shifts = ShowStartedShifts();
                                        foreach ($shifts as $shft) {
                                            ?>
                                            <option value="<?php echo $shft["ShiftNumber"]; ?>"><?php echo $shft["ShiftNumber"] . " - " . $shft["Marshal"]; ?></option>
                                        <?php } ?>
                                    </select>
                                    <label>Select Shift</label>
                                </div>
                            </div>


                            <div class="row">
                                <div class="input-field col s5">
                                    <input placeholder="Specify cancellation reason" id="reason" name="reason" type="text" class="validate">
                                    <label for="reason">Cancellation Reason</label>
                                </div>
                            </div>


                            <div class ="row">

                                <div class="col s4 l4 m4">
                                    <!-- <a  class="waves-effect waves-grey btn-flat"></a>-->
                                    <a type="submit" name="BtnCancelShift" class= "BtnCancelShift waves-effect waves-light btn blue m-b-xs">Cancel Shift</a>
                                </div>


                            </div>


                        </div>
                    </div>
                </div>

            </div>


            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                            <div class="row">
                                <div class="col s6 m6 l6">
                                    <span class="card-title">Shifts Summary</span>
                                </div>

                                <div class="col s3 m3 l3 right-align">
                                    <a class="btnFocusDiv waves-effect waves-light btn red m-b-xs">New Shift</a>
                                </div>

                                <div class="col s2 m2 l2 right-align">
                                    <a class="btnCancelDiv waves-effect waves-light btn red m-b-xs">Cancel Shift</a>
                                </div>


                            </div>

                            <br>
                            <table id="example" class="display responsive-table datatable-example">
                                <thead>
                                    <tr>
                                        <th>Shift #</th>
                                        <th>Marshal</th>
                                        <th>Area</th>
                                        <th>Type</th>
                                        <th>Date Created</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    $Shifts = ShowShifts();
                                    foreach ($Shifts as $Shft) {
                                        $ShiftNum = $Shft["ShiftNumber"];
                                        $Marshal = $Shft["Marshal"];
                                        $CreatedDate = date("d M y", strtotime($Shft["CreatedDate"]));
                                        $Status = $Shft["ShiftStatus"];
                                        $Area = $Shft["AssignedArea"];
                                        $Type = $Shft["ShiftType"];
                                        if (trim($Type) == trim("NB")) {
                                            $MyType = "Vending";
                                        } else if (trim($Type) === trim("B")) {
                                            $MyType = "Collections";
                                        } else {
                                            $MyType = "";
                                        }

                                        if ($Status == "Created") {
                                            $Status = '<div class="chip">' . $Status . '</div>';
                                            $Btn = '';
                                             } else if ($Status == "Started") {
                                            $Status = '<div class="chip blue">' . $Status . '</div>';
                                            $Btn = '<a class="btn-floating btn-small waves-effect waves-light blue" href="ManageReceipts.php?Shft=' .base64_encode($ShiftNum).'" title="View Sales Summary"><i class="small material-icons">visibility</i></a>';
                                        } else if ($Status == "Ended" || $Status == "Closed") {
                                            $Status = '<div class="chip green">' . $Status . '</div>';
                                            $Btn = '<a class="btn-floating btn-small waves-effect waves-light blue" href="ManageReceipts.php?Shft=' .base64_encode($ShiftNum). '" title="View Sales Summary"><i class="small material-icons">visibility</i></a>';
                                            $Btn .= '<a class="btn-floating waves-effect waves-light red" href="ReceiptDist.php?State=' .base64_encode($ShiftNum). '" title="View Receipts Location trail"><i class="tiny material-icons">my_location</i></a>';
                                        } else if ($Status == "Cancelled") {
                                            $Status = '<div class="chip red">' . $Status . '</div>';
                                            $Btn = '';
                                        }
                                        ?>
                                        <tr id='<?php echo $ShiftNum; ?>'>
                                            <td><?php echo $ShiftNum; ?> </td>
                                            <td> <?php echo $Marshal; ?></td>
                                            <td> <?php echo $Area; ?> </td>
                                            <td> <?php echo $MyType; ?> </td>
                                            <td><?php echo $CreatedDate; ?></td>
                                            <td><?php echo $Status; ?></td>
                                            <td><?php echo $Btn; ?></td>
                                        </tr> 

                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>

    </div>
    <div class="left-sidebar-hover"></div>



    <div id="modal2" class="modal">
        <div class="modal-content">
            <h5>Cancel Shift</h5>
            <div class="row">
                <form class="CancelShift col s12" method="post" >
                    <div class="row">
                        <div class="input-field col s8">
                            <input placeholder="Specify cancellation reason" id="reason" name="reason" type="text" class="validate">
                            <label for="phone">Cancel Shift</label>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <div class ="row">
                <div class="col s4 l4 m4">
                    <!-- <a  class="waves-effect waves-grey btn-flat"></a>-->
                    <a type="submit" name="BtnCreateShift" class= "BtnCreateShift waves-effect waves-light btn blue m-b-xs">Create Shift</a>
                </div>
                <div class="col s4 l4 m4">
                    <a href="#!" class="modal-action modal-close waves-effect waves-green btn ">Quit</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Javascripts -->
    <script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
    <script src="assets/plugins/sweetalert/sweetalert.js"></script>
    <script src="assets/plugins/materialize/js/materialize.min.js"></script>
    <script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
    <script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
    <script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/alpha.min.js"></script>
    <script src="assets/js/pages/table-data.js"></script>

    <script>
        $(document).ready(function () {



            $(".FocusDiv").hide();
            $(".cancelDiv").hide();
            $(".area").hide();
            $(".routes").hide();

            $('.btnFocusDiv').click(function () {
                $(".FocusDiv").show("slow");
                $(window).scrollTop($('.FocusDiv').offset().top - 40);
            });

            $(".btnCancelDiv").click(function (ev) {
                ev.preventDefault();
                $(".cancelDiv").show("slow");
                $(window).scrollTop($('.cancelDiv').offset().top - 40);
            });


            var name = '<?php echo $Username; ?>';
            setTimeout(function () {
                Materialize.toast('Welcome ' + name + '!', 4000)
            }, 4000);


                $("#shift_type").change(function(ev)
                {
                    ev.preventDefault();
                    var type = $(this).val();
                    if(type === "WM")
                    {
                        $(".routes").show("slow");
                        $(".area").hide("slow");
                    }
                    else{
                        $(".routes").hide("slow");
                        $(".area").show("slow");
                    }
                });

            
            $(".BtnCancelShift").click(function (ev) {
                ev.preventDefault();
                 var shftNum = $("#shifts").val();
                var cancRsn = $("#reason").val();
                ev.preventDefault();
                $.get("engines/CancShft.php?ShftNum="+shftNum+"&Rzn="+cancRsn,
                        function (response) {
                            var fdbk = $.parseJSON(response);
                            if (fdbk.status === "ok") {
                                swal(fdbk.msg);
                                //alert(fdbk.msg);
                                var delay = 1000;
                                setTimeout(function () {
                                    window.location = "ManageShifts.php";
                                }, delay);
                            } else {
                               // alert(fdbk.msg);
                                swal(fdbk.msg);
                            }
                        });
            });

            $(".BtnCreateShift").click(function (ev) {
                ev.preventDefault();
                var shfttype = $("#shift_type").val();
              
                var arealabel;
                if(shfttype === "WM")
                {
                   arealabel = $("#mega-s").val();
                }
                else{
                     arealabel = $("#parking_area").val();
                }
               
                $.post("engines/CreateShift.php",
                        {
                            marshals: $("#marshals").val(),
                             AreaLabel: arealabel,
                            shift_type: shfttype
                        },
                        function (response) {
                            var fdbk = $.parseJSON(response);
                            if (fdbk.status === "ok") {
                                //alert(fdbk.msg);
                                swal(fdbk.msg);
                                var delay = 1000;
                                setTimeout(function () {
                                    location.reload();
                                }, delay);
                            } else {
                                //alert(fdbk.msg);
                                swal(fdbk.msg);
                            }
                        });
            });

        });
    </script>
</body>
</html>