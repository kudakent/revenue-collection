<?php
require 'DB/DBAPI.php';
if (!Is_Logged_In()) {
    redirect("login.php");
} else {
    $UserData = UserDetails($_SESSION["acc"]);
    $Username = $_SESSION["Username"];
    $FirstName = $UserData[0]["UserFirstName"];
    $LastName = $UserData[0]["UserSurname"];
    $UserType = $UserData[0]["UserType"];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- Title -->
        <title>Axis || <?php echo basename($_SERVER['PHP_SELF']); ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />

        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="assets/plugins/materialize/css/materialize.min.css"/>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">
        <link href="assets/plugins/datatables/css/jquery.dataTables.min.css" rel="stylesheet">


        <!-- Theme Styles -->
        <link href="assets/css/alpha.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <?php require 'config.php'; ?>

        <main class="mn-inner">
            <div class="row FocusDiv">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                            <h5>New Slot</h5>
                            <div class="row">

                                <div class="input-field col s5">
                                    <input id="Label" name="Area" placeholder="Enter area label" type="text" class="validate">
                                    <label for="Area">Parking Slot Label</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s5">
                                    <input id="AreaA" name="Area" value="Sam Nujoma, Harare, Zimbabwe" type="text" class="validate geocomplete">
                                    <label for="Area">Between Street/Avenue</label>
                                </div>

                                <div class="input-field col s1">
                                    <h6> and </h6>
                                </div>

                                <div class="input-field col s5">
                                    <input id="AreaB" name="Area" value="3rd Street, Harare, Zimbabwe" type="text" class="validate geocomplete">
                                    <label for="Area"> Street/Avenue</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s5">
                                    <input id="AlongArea" name="Area" value="Selous Avenue, Harare, Zimbabwe" type="text" class="validate geocomplete">
                                    <label for="Area">Along Street/Avenue</label>
                                </div>



                            </div>

                            <div class ="row">

                                <div class="col s4 l4 m4">
                                    <!-- <a  class="waves-effect waves-grey btn-flat"></a>-->
                                    <a type="submit" name="BtnCreateSlot" class= "BtnCreateSlot waves-effect waves-light btn blue m-b-xs">Create Slot</a>
                                </div>


                            </div>


                        </div>
                    </div>
                </div>

            </div>


            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                            <div class="row">
                                <div class="col s6 m6 l6">
                                    <span class="card-title">Active Slots</span>
                                </div>

                                <div class="col s6 m6 l6 right-align">
                                    <a class="btnFocusDiv waves-effect waves-light btn red m-b-xs">Create Slot</a>
                                </div>

                            </div>

                            <br>
                            <table id="example" class="display responsive-table datatable-example">
                                <thead>
                                    <tr>
                                        <th>Slots</th>
                                        <th>Point A</th>
                                        <th>Point B</th>
                                        <th>Point C</th>
                                        <th>Sales ($)</th>
                                        <th>Date Created</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    $Slots = GetSlots();
                                    foreach ($Slots as $Slt) {
                                        $Label = $Slt["AreaLabel"];
                                        $PointA = $Slt["PointA"];
                                        $PointB = $Slt["PointB"];
                                        $AlongSt = $Slt["PointAlong"];
                                        $WrthyOfSales = GetSlotsSales($Label);
                                        $Sales = $WrthyOfSales[0]["Sls"];
                                        if (empty($Sales)) {
                                            $Sales = 0.00;
                                        }
                                        $CreatedDate = date("d M y", strtotime($Slt["DateCreated"]));
                                        ?>
                                        <tr id='<?php echo $Label; ?>'>
                                            <td><?php echo $Label; ?> </td>
                                            <td><?php echo $PointA; ?> </td>
                                            <td> <?php echo $PointB; ?></td>
                                            <td><?php echo $AlongSt; ?></td>
                                            <td><?php echo round($Sales,2); ?></td>
                                            <td><?php echo $CreatedDate; ?></td>
                                        </tr> 

                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>

    </div>
    <div class="left-sidebar-hover"></div>


    <!-- Javascripts -->
    <script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
    <script src="assets/plugins/sweetalert/sweetalert.js"></script>
    <script src="assets/plugins/materialize/js/materialize.min.js"></script>
    <script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
    <script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
    <script src="assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/alpha.min.js"></script>
    <script src="assets/js/pages/table-data.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyBXYnIsUWziNd4F48t63Nh9irV2sskLPhY&amp;libraries=places"></script>
    <script src="assets/js2/jquery.geocomplete.js"></script>
    <script>
        $(document).ready(function () {

            $(".geocomplete").geocomplete({
                details: ".details",
                detailsScope: '.location',
                types: ["geocode", "establishment"]
            });

            $(".FocusDiv").hide();

            $('.btnFocusDiv').click(function () {
                $(".FocusDiv").show("slow");
                $(window).scrollTop($('.FocusDiv').offset().top - 40);
            });


            var name = '<?php echo $Username; ?>';
            setTimeout(function () {
                Materialize.toast('Welcome ' + name + '!', 4000)
            }, 4000);

         

            $(".BtnCreateSlot").click(function (ev) {
                ev.preventDefault();
                $.post("engines/CreateSlot.php",
                        {
                           
                            AreaA: $("#AreaA").val(),
                            AreaB: $("#AreaB").val(),
                            Alongarea: $("#AlongArea").val(),
                            AreaLabel: $("#Label").val()
                        },
                        function (response) {
                            var fdbk = $.parseJSON(response);
                            if (fdbk.status === "ok") {
                               // alert(fdbk.msg);
                                swal(fdbk.msg);
                                var delay = 1000;
                                setTimeout(function () {
                                    location.reload();
                                }, delay);
                            } else {
                               // alert(fdbk.msg);
                                swal(fdbk.msg);
                            }
                        });
            });

        });
    </script>
</body>
</html>