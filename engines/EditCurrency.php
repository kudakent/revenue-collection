<?php
require '../DB/DBAPI.php';


$currency = $_POST['currency_name'];
$exchangerate = $_POST['ex_rate'];
$ExchangeRateID = $_POST['id'];
$curr_code = $_POST['currency_code'];

if($currency=="" || $exchangerate==""){
    $error = ' Currency name or Exchange rate empty. Please fill all fields!';

}
else{
  $edit_curr =  update_exchage_rate($currency,$curr_code,$exchangerate,$ExchangeRateID);
  
    if($edit_curr['status']=="ok"){
        $msg = 'Currency edited successfully, please wait as the system effects changes.';
    }
    else{
        $error = ' Edit Failed. Error: '.$edit_curr['status'];
    }
}


if(isset($msg)){
    $result["status"]="ok";
     $result["msg"]=$msg;
}elseif(isset($error)){
      $result["status"]="fail";
     $result["msg"]=$error;
}

echo json_encode($result);

