<?php

require '../DB/ReportsAPI.php';

$dom = new DOMDocument("1.0");
$node = $dom->createElement("markers");
$parnode = $dom->appendChild($node);

header("Content-type: text/xml");

$meterData = getRatePayerDistribution();
foreach($meterData as $vals){
if(($vals['Latitude']=="0" || $vals['Longitude']=="0.00") && ($vals['Latitude']=="0" || $vals['Longitude']=="0")){

}
else{
$node = $dom->createElement("marker");
  $newnode = $parnode->appendChild($node);

  $newnode->setAttribute("meterNum", $vals['MeterNum']);
  $newnode->setAttribute("AccNum", $vals['AccNum']);
  $newnode->setAttribute("lat", $vals['Latitude']);
  $newnode->setAttribute("lng", $vals['Longitude']);
  $newnode->setAttribute("balance", $vals['Balance']);
  $newnode->setAttribute("CustomerName", $vals['CustomerName']);
  $newnode->setAttribute("LastReading", $vals['Reading']);
  $newnode->setAttribute("LastReadDate", date("d M y",strtotime($vals['ReadingDate'])));
  if($vals['Balance']>0){
    $newnode->setAttribute("Type", "O");
  }
  else{
        $newnode->setAttribute("Type", "P");
  }
}
}
echo $dom->saveXML();




 