<?php
require '../DB/DBAPI.php';
$code = $_POST["code"];
$desc = $_POST["desc"];
$UOM = $_POST["uom"];
$untPri = $_POST["unitprice"];
$TaxCode = $_POST["taxcode"];
$ParentCode = $_POST["promuninccode"];

if($TaxCode == "A"){
$excUntPri = round(($untPri/1.15),2);
}
else{
    $excUntPri = $untPri;
}

if(empty($ParentCode) || empty($code) || empty($desc) || empty($UOM) || empty($excUntPri) || empty($untPri)  || empty($TaxCode))
{
      $rslt["msg"] = 'Please fill in all fields.'; 
      $rslt["status"] = "error";
}
else{
     $prod = get_Prod_name_code($code,$desc);
    if($prod['status']=="exist"){
        $rslt["msg"] = 'Either your service name or service code exists already, please verify your code or service name.'; 
      $rslt["status"] = "error";
         }
         else{
             $NewService = create_product($ParentCode,$code, $desc, $UOM, $excUntPri, $untPri, $TaxCode);
             if($NewService["status"] == "ok")
             {
                 $rslt["msg"] = 'Service has been set successfully created'; 
                    $rslt["status"] = "ok";
             }
             else{
                 $rslt["msg"] = 'Failed to create service. Error '.$NewService["status"]; 
      $rslt["status"] = "error";
             }
         }
}

echo json_encode($rslt);