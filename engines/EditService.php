<?php

require '../DB/DBAPI.php';
$id = $_GET["id"];
$ParentCode = $_POST["promuninccode"];
$code = $_POST["code"];
$desc = $_POST["desc"];
$UOM = $_POST["uom"];
$untPri = $_POST["unitprice"];
$TaxCode = $_POST["taxcode"];
$SvcInfo = ShowServiceData($id);

if ($UOM == ""){
     
     $UOM = $SvcInfo[0]["UOM"];
}

if ($TaxCode == ""){
   
     $TaxCode = $SvcInfo[0]["TaxCode"];
}

if($ParentCode == ""){
    $ParentCode = $SvcInfo[0]["ParentCode"];
}

if($TaxCode == "A")
    {
$excUntPri = round(($untPri/1.15),2);
} 
else{
    $excUntPri = $untPri;
}

if(empty($code) || empty($desc) || empty($excUntPri) || empty($untPri))
{
      $rslt["msg"] = 'Please fill in all fields.'; 
      $rslt["status"] = "error";
}
else{
    // $prod = GetProdCodeExcl($code);
    
           $EditService =  EditProduct($ParentCode,$code, $desc, $UOM, $excUntPri, $untPri, $TaxCode,$id);
             if($EditService["status"] == "ok")
             {
                 $rslt["msg"] = 'Service has been updated successfully!'; 
                    $rslt["status"] = "ok";
             }
             else{
                 $rslt["msg"] = 'Failed to update service. Error '.$EditService["status"]; 
      $rslt["status"] = "error";
             }
         }


echo json_encode($rslt);
