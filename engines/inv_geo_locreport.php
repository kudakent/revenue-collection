<?php

require '../DB/ReportsAPI.php';

$dom = new DOMDocument("1.0");
$node = $dom->createElement("markers");
$parnode = $dom->appendChild($node);

header("Content-type: text/xml");

$inv_values = get_invoice_coord();
foreach($inv_values as $vals){
if(($vals['GPSLatitude']=="0" || $vals['GPSLatitude']=="0.00") && ($vals['GPSLongitude']=="0" || $vals['GPSLongitude']=="0")){

}
else{
$node = $dom->createElement("marker");
  $newnode = $parnode->appendChild($node);

  $newnode->setAttribute("inv_number", $vals['InvoiceNum']);
  $newnode->setAttribute("lat", $vals['GPSLatitude']);
  $newnode->setAttribute("lng", $vals['GPSLongitude']);
  $newnode->setAttribute("Total", $vals['InvoiceTotal']);
  $newnode->setAttribute("CustomerName", $vals['CustomerName']);
  if($vals['InvoiceTotal']>0){
    $newnode->setAttribute("Type", "I");
  }
  else{
        $newnode->setAttribute("Type", "R");
  }
}
}
echo $dom->saveXML();




 