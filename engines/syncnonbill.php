<?php

require '../DB/DBAPI.php';
require '../DB/ODBCAPI.php';

//global variables 
$opcode = 1;
$ref = 0;
$paytype = "C";
$seqno = 1;
$cash = 99999.00;
$recstatus = "U";
$recdate = date("Y-m-d");



$State = $_GET["state"];
if ($State == "one") {
    $TransID = $_GET["recid"];
    $AccDetails = GetReceiptDetails($TransID);
    $mcno = 5;
    $cash = 99999.00;
    $recstatus = "U";
    $recdate = date("Y-m-d");

    $code = "ZZ";
    $acc = $AccDetails[0]["AccountNumber"];
    $amount = $AccDetails[0]["AmountPaid"];
    $recno = 1; // this increments as we add to 
    $opcode = 1;
    $ref = 0;
    $paytype = "C";
    $seqno = 1;
    $recname = $TransID;



    $CreateRec = CreateMunrct($code, $acc, $amount, $mcno, $recdate, $recno, $opcode, $ref, $paytype, $seqno, $recstatus, $recname);
//create as many receipts as you want but with single ,machone number
    $CreateRecContrl = CreateMunrctctr($mcno, $cash, $recstatus, $recdate);

    if (($CreateRec["status"] == "ok") && ($CreateRecContrl["status"] == "ok")) {

        $MunRctSyncStatus = $CreateRec["status"];
        $MunRctCtrSyncStatus = $CreateRecContrl["status"];
        UpdateSyncStatus($MunRctSyncStatus, $MunRctCtrSyncStatus, $TransID);

        $rslt["msg"] = "Rec created successfully in promun.";
        $rslt["status"] = "ok";
    } else {

        $MunRctSyncStatus = $CreateRec["status"];
        $MunRctCtrSyncStatus = $CreateRecContrl["status"];
        UpdateSyncStatus($MunRctSyncStatus, $MunRctCtrSyncStatus, $TransID);

        $rslt["msg"] = "Failed to create a receipt. rec error " . $CreateRec["status"] . " and control error: " . $CreateRecContrl["status"];
        $rslt["status"] = "fail";
    }
    echo json_encode($rslt);
} else if ($State == "two") {
    //getReceiptsToSync
    $AvailRecs = GetActReceiptsToSync();
    $i = 1;
    foreach ($AvailRecs as $Recs) {

        $Prod = $Recs["ProductName"];
        $arr = explode("#", $Prod, 2);
        $Pcode = $arr[0];
        $SvcMaster = getIncomeCodeDet($Pcode);
        $code = $SvcMaster[0]["IncCode"];
        $acc = $SvcMaster[0]["GLAlloc"];
        
        $mcno = $i;
        $amount = $Recs["ProductTotal"];
        $recno = $Recs["InvoiceID"]; // this increments as we add to 
        
        $recname = $Pcode;
        $CreateRec = CreateMunrct($code, $acc, $amount, $mcno, $recdate, $recno, $opcode, $ref, $paytype, $seqno, $recstatus, $recname);

        $MunRctSyncStatus = $CreateRec["status"];
        if($MunRctSyncStatus == "ok"){
            $syncstatus = 1;
        }else{
            $syncstatus = 2;
        }
         UpdateRecSyncStatus($syncstatus,$recno);
    }
    if ($CreateRec["status"] == "ok") {
       
            $rslt["msg"] = "Receipts posted successfully in promun.";
            $rslt["status"] = "ok";
        }  else {
        $rslt["msg"] = "Failed to create receipting records. Error: " . $CreateRec["status"];
        $rslt["status"] = "fail";
    }

   
    echo json_encode($rslt);
}





