<?php
require '../DB/DBAPI.php';
@$AreaPointA = $_POST["AreaA"];
@$AreaPointB = $_POST["AreaB"];
@$AreaAlongSt = $_POST["Alongarea"];
@$AreaLabel = $_POST["AreaLabel"];


if(empty($AreaLabel) )
{
      $rslt["msg"] = 'Please fill in all required fields - area label'; 
      $rslt["status"] = "error";
}
else{

       $NewSlot = CreateSlot($AreaPointA,$AreaPointB,$AreaAlongSt,$AreaLabel);
       if($NewSlot["status"]=="ok")
       {
        $rslt["msg"] = 'Slot has been created succefully. The page will reload to effect changes.'; 
      $rslt["status"] = "ok";
    }
    else
    {
        $rslt["msg"] = 'Failed to create slot. Error: '.$NewSlot["status"]; 
      $rslt["status"] = "error";
    }
}

echo json_encode($rslt);