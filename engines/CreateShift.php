<?php
require '../DB/DBAPI.php';
$ShiftData = $_POST["marshals"];

@$AreaLabel = $_POST["AreaLabel"];
$ShiftType = $_POST["shift_type"];
//$AreaLabel = "Test";
if(is_array($AreaLabel))
{
   @$AreaLabel = implode(",",$AreaLabel); 
}


if(empty($ShiftData) || empty($ShiftType))
{
      $rslt["msg"] = 'Please specify marshal name, shift type and  area to be covered'; 
      $rslt["status"] = "error";
}
else{
    $MarshalArray  = explode(',', $ShiftData);
    $MarshalID = $MarshalArray[0];
    $MarshalName = $MarshalArray[1];
    $NewShift = CreateShift($MarshalID,$MarshalName,$AreaLabel,$ShiftType);
    if($NewShift["status"]=="ok")
    {
       
        $rslt["msg"] = 'Shift with number '.$NewShift["sheetNumber"].' created Succesfully. To start please sync with mobile device.'; 
      $rslt["status"] = "ok";
    }
    else
    {
        $rslt["msg"] = 'Failed to create shift. Error: '.$NewShift["status"]; 
      $rslt["status"] = "error";
    }
}

echo json_encode($rslt);