<?php
require '../DBAPI/DB.php';
$old_pass = $_POST['old_password'];
$new_pass = $_POST['new_password'];
$confirm = $_POST['confirm_password'];
$user_id = $_SESSION['acc'];
$lk_id = base64_encode($user_id);

$det = get_ext_userdetails($user_id);
if($old_pass=="" || $new_pass=="" || $confirm==""){
    $result['msg']='Please fill up all password forms! Old password, new password and confirmed password fields can not be blank';
    $result['log']='fail';
}
elseif($old_pass!=$det[0]['Password']){
  $result['msg']='Old password not correct! Please ensure your old password matches your currently stored password!';
    $result['log']='fail';  
}
elseif(strlen($new_pass)<5 ){
    $result['msg']='New password: Please make sure the password length is greater or equal to 4 characters.';
    $result['log']='fail';  
}
elseif($new_pass!=$confirm){
     $result['msg']='New password not matching the confirmation password!';
    $result['log']='fail'; 
}
else{
  $edit_pass =  edit_user_pass($new_pass,$user_id);
  if($edit_pass['status']=="ok"){
    $result['msg']='Password successfully updated. Click <a href="base_pages_lock.php?lkid='.$lk_id.'">here</a> to unlock account and login again to effect changes';
    $result['log']='pass';
  }else{
      $result['msg']='Could not update password! ERROR: '.$edit_pass['status'];
    $result['log']='fail';
  }
}

echo json_encode($result);