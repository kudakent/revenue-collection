

<html lang="en">
    <head>
        
        <!-- Title -->
        <title>AxiRevCol | Login</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Aximos Park, Parking Solution" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />
        
        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="assets/plugins/materialize/css/materialize.min.css"/>
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">        

        	
        <!-- Theme Styles -->
        <link href="assets/css/alpha.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>
        
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body class="signin-page">
        <div class="loader-bg"></div>
    
        <div class="mn-content valign-wrapper">
            <main class="mn-inner container ">
                <div class="valign">
                      <div class="row">
                          <div class="col s12 m10 l4 offset-l4 offset-m1">
                              <div class="card white darken-1">
                             
                                  <div class="card-content">
                                  <img src="assets/images/logo.png" width="190px" class="circle lock-screen-image" alt="">
                                      <div class="row">
                                          <form method="post" action="" class="col s12 FormLogin">
                                           <div class="input-field col s12">
                                               <input id="username" type="text" name="username">
                                                   <label for="username">Username</label>
                                               </div>
                                               <div class="input-field col s12">
                                                   <input id="password" type="password" name="password">
                                                   <label for="password">Password</label>
                                               </div>
                                               <div class="row right-align m-t-sm">
                                                   <div class="col-sm-6">
                                                   <div class="new badge red-text response "> </div>  
                                                   </div>
                                                   <br>
                                                   <div class="col-sm-6">
                                                  <!-- <a  class="waves-effect waves-grey btn-flat"></a>-->
                                                  <a type="submit" name="loginbutton" class= "btnlogin waves-effect waves-light btn teal">Login</a>
                                                   </div>
                                               </div>
                                           </form>
                                      </div>
                                  </div>
                              </div>
                          </div>
                    </div>
                </div>
            </main>
        </div>
        
        
        <!-- Javascripts -->
        <script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
        <script src="assets/plugins/materialize/js/materialize.min.js"></script>
        <script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
        <script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
        <script src="assets/js/alpha.min.js"></script>
        
    </body>
</html>

<script>
    $(document).ready(function(){
        
        $(".btnlogin").click(function(ev){
            ev.preventDefault();
          $.post("engines/login.php",$(".FormLogin").serialize(),
          function(resp){
              console.log(resp);
              var fdbk = $.parseJSON(resp);
             
              if(fdbk.status === "Ok"){
                window.location = fdbk.msg;
              }
              else if(fdbk.status === "Error"){
                  $(".response").html(fdbk.msg);
              }
          });
            
        });
        
    });
    </script>
    